#ifndef APP_H
#define APP_H

class Mixer;

class App {
public:
    App();
    ~App();

    void render();
    void init();

private:
    bool show_demo_window;
    bool show_another_window;
    Mixer *mixer;
};


#endif 