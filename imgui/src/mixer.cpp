#include "mixer.h"

// #define DR_WAV_IMPLEMENTATION
// #include "miniaudio/extras/dr_wav.h" /* Enables WAV decoding. */

// #define MINIAUDIO_IMPLEMENTATION
// #include "miniaudio/miniaudio.h"

#include "track.h"
#include "rtaudio/RtAudio.h"

#include <SDL2/SDL.h>

#include <sndfile.h>

#include <stdio.h>

namespace
{
    // void data_callback(ma_device *pDevice, void *pOutput, const void *pInput, ma_uint32 frameCount)
    // {
    //     ma_decoder *pDecoder = (ma_decoder *)pDevice->pUserData;
    //     if (pDecoder == NULL)
    //     {
    //         return;
    //     }

    //     ma_decoder_read_pcm_frames(pDecoder, pOutput, frameCount);

    //     (void)pInput;
    // }

    // int playback_miniaudio(const char *filename)
    // {
    //     ma_decoder decoder;
    //     ma_result result = ma_decoder_init_file(filename, NULL, &decoder);
    //     if (result != MA_SUCCESS)
    //     {
    //         return -2;
    //     }

    //     ma_device_config config = ma_device_config_init(ma_device_type_playback);
    //     config.playback.format = decoder.outputFormat;
    //     config.playback.channels = decoder.outputChannels;
    //     config.sampleRate = decoder.outputSampleRate;
    //     config.dataCallback = data_callback;
    //     config.pUserData = &decoder;

    //     ma_device device;
    //     if (ma_device_init(NULL, &config, &device) != MA_SUCCESS)
    //     {
    //         printf("Failed to open playback device.\n");
    //         ma_decoder_uninit(&decoder);
    //         return -3;
    //     }

    //     if (ma_device_start(&device) != MA_SUCCESS)
    //     {
    //         printf("Failed to start playback device.\n");
    //         ma_device_uninit(&device);
    //         ma_decoder_uninit(&decoder);
    //         return -4;
    //     }

    //     printf("Press Enter to quit...");
    //     getchar();

    //     ma_device_uninit(&device);
    //     ma_decoder_uninit(&decoder);
    // }

    // int init_miniaudio()
    // {
    //     ma_result result;
    //     ma_context context;
    //     ma_device_info *pPlaybackDeviceInfos;
    //     ma_uint32 playbackDeviceCount;
    //     ma_device_info *pCaptureDeviceInfos;
    //     ma_uint32 captureDeviceCount;
    //     ma_uint32 iDevice;

    //     if (ma_context_init(NULL, 0, NULL, &context) != MA_SUCCESS)
    //     {
    //         printf("Failed to initialize context.\n");
    //         return -2;
    //     }

    //     result = ma_context_get_devices(&context, &pPlaybackDeviceInfos, &playbackDeviceCount, &pCaptureDeviceInfos, &captureDeviceCount);
    //     if (result != MA_SUCCESS)
    //     {
    //         printf("Failed to retrieve device information.\n");
    //         return -3;
    //     }

    //     printf("Playback Devices\n");
    //     for (iDevice = 0; iDevice < playbackDeviceCount; ++iDevice)
    //     {
    //         printf("    %u: %s\n", iDevice, pPlaybackDeviceInfos[iDevice].name);
    //     }

    //     printf("\n");

    //     printf("Capture Devices\n");
    //     for (iDevice = 0; iDevice < captureDeviceCount; ++iDevice)
    //     {
    //         printf("    %u: %s\n", iDevice, pCaptureDeviceInfos[iDevice].name);
    //     }

    //     ma_context_uninit(&context);
    //     return 0;
    // }

    // Two-channel sawtooth wave generator.
    int playStream(void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
            double streamTime, RtAudioStreamStatus status, void *userData)
    {
        // SDL_Log("Streaming...");
        unsigned int i, j;
        double *buffer = (double *)outputBuffer;
        Mixer *mixer = (Mixer*)userData;
        // SNDFILE *file = (SNDFILE *)userData;
        if (status)
            std::cout << "Stream underflow detected!" << std::endl;

        // TODO: read all tracks
        // sf_readf_double(audio->file, buffer, nBufferFrames);

        // Write interleaved audio data.
        // for (i = 0; i < nBufferFrames; i++)
        // {
        //     for (j = 0; j < 2; j++)
        //     {
        //         *buffer++ = lastValues[j];
        //         lastValues[j] += 0.005 * (j + 1 + (j * 0.1));
        //         if (lastValues[j] >= 1.0)
        //             lastValues[j] -= 2.0;
        //     }
        // }
        return 0;
    }

    // void init_rtaudio(RawAudio *audio)
    // {
    //     RtAudio dac;
    //     if (dac.getDeviceCount() < 1)
    //     {
    //         std::cout << "\nNo audio devices found!\n";
    //         exit(0);
    //     }
    //     RtAudio::StreamParameters parameters;
    //     parameters.deviceId = dac.getDefaultOutputDevice();
    //     parameters.nChannels = 2;
    //     parameters.firstChannel = 0;
    //     unsigned int sampleRate = 44100;
    //     unsigned int bufferFrames = 256; // 256 sample frames
    //     double data[2];
    //     try
    //     {
    //         dac.openStream(&parameters, NULL, RTAUDIO_FLOAT64,
    //                        sampleRate, &bufferFrames, &play, (void *)audio);
    //         dac.startStream();
    //     }
    //     catch (RtAudioError &e)
    //     {
    //         e.printMessage();
    //         exit(0);
    //     }

    //     char input;
    //     std::cout << "\nPlaying ... press <enter> to quit.\n";
    //     std::cin.get(input);
    //     try
    //     {
    //         // Stop the stream
    //         dac.stopStream();
    //     }
    //     catch (RtAudioError &e)
    //     {
    //         e.printMessage();
    //     }
    //     if (dac.isStreamOpen())
    //         dac.closeStream();
    // }

} // namespace

Mixer::Mixer()
    : current_audio(NULL), dac(new RtAudio())
{
    // init_miniaudio();
    // init_rtaudio();
    if (dac->getDeviceCount() < 1)
    {
        std::cout << "\nNo audio devices found!\n";
        // TODO: remove from the constructor?
        exit(0);
    }
}

Mixer::~Mixer()
{
    reset();
    delete dac;
}

// void Mixer::close_file()
// {
//     if (current_audio != NULL)
//         sf_close(current_audio->file);
// }

void Mixer::reset()
{
    // close_file();
    for (int i = 0; i < tracks.size(); i++) {
        delete tracks[i];
    }

    if (dac->isStreamOpen())
        dac->closeStream();
}

void Mixer::stop()
{
    try
    {
        dac->stopStream();
    }
    catch (RtAudioError &e)
    {
        e.printMessage();
    }

    reset();
}

// void Mixer::load(const std::string &wave)
// {
//     this->reset();

//     this->current_audio = load_file(wave.c_str());
// }


void Mixer::backward() {
    for (int i = 0; i < tracks.size(); i++) {
        tracks[i]->backward();
    }
}

void Mixer::play()
{
    if (this->tracks.empty()) {
        return;
    }

    if (dac->isStreamOpen()) {
        dac->closeStream();
    }

    this->backward();
    
    RtAudio::StreamParameters parameters;
    parameters.deviceId = dac->getDefaultOutputDevice();
    parameters.nChannels = 2;
    parameters.firstChannel = 0;
    unsigned int sampleRate = 44100;
    unsigned int bufferFrames = 256; // 256 sample frames
    try
    {
        dac->openStream(&parameters, NULL, RTAUDIO_FLOAT64,
                        sampleRate, &bufferFrames, &playStream, (void *)this);
        dac->startStream();
    }
    catch (RtAudioError &e)
    {
        e.printMessage();
        exit(0);
    }
}



Track* Mixer::add_track(const std::string &filename) {
    Track *track = new Track(filename);
    tracks.push_back(track);
    return track;
}