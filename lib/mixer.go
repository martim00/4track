package lib

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	"github.com/gen2brain/malgo"
	sox "github.com/krig/go-sox"
)

var (
	SampleRate       = uint32(44100)
	Precision        = 32
	Channels         = uint32(2)
	SoxEncodingSign2 = 1 // sox enum for signed linear 2's comp encoding
	MixedFile        = "mixed.wav"
	RecPlaybackFile  = "rec-playback.wav"
)

type Track struct {
	ID           int
	Samples      []byte
	SamplesCount uint32
}

func NewTrack(id int) *Track {
	return &Track{
		ID:           id,
		SamplesCount: 0,
	}
}

type Mixer struct {
	// recordSamples      []byte
	// recordSamplesCount uint32
	Tracks         []*Track
	numberOfTracks int
	ArmedTrack     *Track
}

func NewMixer() *Mixer {
	mixer := Mixer{
		numberOfTracks: 4,
	}
	// fmt.Println("creating mixer")
	for i := 0; i < mixer.numberOfTracks; i++ {
		mixer.Tracks = append(mixer.Tracks, NewTrack(i))
	}

	return &mixer
}

func (m *Mixer) ArmRecord(track int) error {
	if track < 0 || track > len(m.Tracks) {
		return fmt.Errorf("track out of range")
	}
	m.ArmedTrack = m.Tracks[track]
	return nil
}

func (m *Mixer) preparePlayback() bool {
	// prepare a mix with everything but the current armed track
	files := m.getTracksOutput(true)
	fmt.Println(files)
	return m.mix(files, RecPlaybackFile)
}

func (m *Mixer) Record(stopRec chan bool) {

	if m.ArmedTrack == nil {
		panic("No track armed. This shouldnt happen")
	}

	go m.PlayRecPlayback(stopRec)

	ctx, err := malgo.InitContext(nil, malgo.ContextConfig{}, func(message string) {
		fmt.Printf("LOG <%v>\n", message)
	})
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer func() {
		_ = ctx.Uninit()
		ctx.Free()
	}()

	deviceConfig := malgo.DefaultDeviceConfig()
	deviceConfig.Format = malgo.FormatS32
	deviceConfig.Channels = Channels
	deviceConfig.SampleRate = SampleRate
	deviceConfig.Alsa.NoMMap = 1

	m.ArmedTrack.Samples = make([]byte, 0)
	m.ArmedTrack.SamplesCount = 0

	// sizeInBytes := uint32(malgo.SampleSizeInBytes(deviceConfig.Format))
	sizeInBytes := uint32(Precision / 8)
	onRecvFrames := func(framecount uint32, pSamples []byte) {
		sampleCount := framecount * deviceConfig.Channels * sizeInBytes

		newCapturedSampleCount := m.ArmedTrack.SamplesCount + sampleCount

		m.ArmedTrack.Samples = append(m.ArmedTrack.Samples, pSamples...)

		m.ArmedTrack.SamplesCount = newCapturedSampleCount
	}

	// fmt.Println("Recording...")
	captureCallbacks := malgo.DeviceCallbacks{
		Recv: onRecvFrames,
	}
	device, err := malgo.InitDevice(ctx.Context, malgo.Capture, nil, deviceConfig, captureCallbacks)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	defer device.Uninit()

	err = device.Start()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	for {
		select {
		case <-stopRec:

			m.dumpTrack(m.ArmedTrack)
			return
			// fmt.Println("Stopped...")
		}
	}
}

func (m *Mixer) dumpTrack(track *Track) {
	filename := fmt.Sprintf("raw%d", track.ID)
	raw := fmt.Sprintf("%s.raw", filename)
	wav := fmt.Sprintf("%s.wav", filename)

	err := ioutil.WriteFile(raw, m.ArmedTrack.Samples, 0644)
	if err != nil {
		panic(err)
	}
	args := []string{"-r", fmt.Sprintf("%d", SampleRate), "-e", "signed-integer",
		"-b", fmt.Sprintf("%d", Precision), "-c", fmt.Sprintf("%d", Channels), raw, wav}
	// fmt.Println(args)
	runSox(args...)

}

func (m *Mixer) HasArmedRecord() bool {
	return m.ArmedTrack != nil
}

// Flow data from in to out via the samples buffer
func flow(in, out *sox.Format, samples []sox.Sample) {
	n := uint(len(samples))
	for number_read := in.Read(samples, n); number_read > 0; number_read = in.Read(samples, n) {
		out.Write(samples, uint(number_read))
	}
}

func playCallback(allDone bool) int {
	// fmt.Println("callbac")
	// if allDone {
	// 	fmt.Println("finished")
	// }

	return 0
}

func readMemInput(track *Track) *sox.Format {
	signalInfo := sox.NewSignalInfo(float64(SampleRate), uint(Channels), uint(Precision), 0, nil)
	encoding := sox.NewEncodingInfo(SoxEncodingSign2, uint(Precision), 0, false)
	in := sox.OpenMemRead0(track.Samples, signalInfo, encoding, "raw")
	if in == nil {
		log.Fatal("Failed to open memory buffer for reading")
	}
	return in
}

func (m *Mixer) readAllInput() []*sox.Format {
	var result []*sox.Format
	for _, track := range m.Tracks {
		if track.SamplesCount > 0 {
			result = append(result, readMemInput(track))
		}
	}

	return result
}

func (m *Mixer) getTracksOutput(removeArmed bool) []string {
	var result []string
	for i, track := range m.Tracks {

		if track.SamplesCount == 0 {
			continue
		}

		if removeArmed && track == m.ArmedTrack {
			continue
		}

		filename := fmt.Sprintf("raw%d.wav", i)
		if FileExists(filename) {
			result = append(result, filename)
		}
	}

	return result
}

func runSox(args ...string) {
	cmd := exec.Command("sox", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	err := cmd.Run()
	if err != nil {
		fmt.Printf("Failed to run sox. %s\n", err.Error())
		os.Exit(1)
	}
}

func (m *Mixer) mix(files []string, output string) bool {
	// if there's no file input we should just return
	if len(files) == 0 {
		return false
	}

	DeleteFile(output)

	// if there's only one input we should just copy to the mixed output
	if len(files) == 1 {
		CopyFile(files[0], output)
		return true
	}

	// otherwise we run sox to mix all the inputs
	args := []string{"-m"}
	args = append(args, files...)
	args = append(args, output)
	runSox(args...)
	return true
}

func (m *Mixer) Mix() bool {

	files := m.getTracksOutput(false)
	return m.mix(files, MixedFile)
}

// func (m *Mixer) Mix(stop chan bool) (chan bool, chan string) {

// 	stoppedChan := make(chan bool)
// 	progressChan := make(chan string)

// 	go func() {

// 		if !sox.Init() {
// 			log.Fatal("Failed to initialize SoX")
// 		}
// 		defer sox.Quit()

// 		all := m.readAllInput()
// 		in := all[0]

// 		// Open the output device: Specify the output signal characteristics.
// 		// Since we are using only simple effects, they are the same as the
// 		// input file characteristics.
// 		// Using "alsa" or "pulseaudio" should work for most files on Linux.
// 		// On other systems, other devices have to be used.
// 		out := sox.OpenWrite("default", in.Signal(), nil, "coreaudio")
// 		if out == nil {
// 			log.Fatal("Failed to open output device")
// 		}
// 		// Close the output device before exiting
// 		defer out.Release()

// 		// Create an effects chain: Some effects need to know about the
// 		// input or output encoding so we provide that information here.
// 		chain := sox.CreateEffectsChain(in.Encoding(), out.Encoding())
// 		// Make sure to clean up!
// 		defer chain.Release()

// 		// The first effect in the effect chain must be something that can
// 		// source samples; in this case, we use the built-in handler that
// 		// inputs data from an audio file.
// 		e := sox.CreateEffect(sox.FindEffect("input"))
// 		e.Options(in)
// 		// This becomes the first "effect" in the chain
// 		chain.Add(e, in.Signal(), in.Signal())
// 		e.Release()

// 		// lastIn := in

// 		for i, input := range all {
// 			if i == 0 {
// 				continue
// 			}

// 			inp := input

// 			e := sox.CreateEffect(sox.FindEffect("input"))
// 			e.Options(inp)
// 			chain.Add(e, inp.Signal(), inp.Signal())
// 			// lastIn = inp
// 			e.Release()
// 		}

// 		// Create the `vol' effect, and initialise it with the desired parameters:
// 		// e = sox.CreateEffect(sox.FindEffect("vol"))
// 		// e.Options("3dB")
// 		// // Add the effect to the end of the effects processing chain:
// 		// chain.Add(e, in.Signal(), in.Signal())
// 		// e.Release()

// 		// Create the `flanger' effect, and initialise it with default parameters:
// 		// e = sox.CreateEffect(sox.FindEffect("flanger"))
// 		// e.Options()
// 		// chain.Add(e, in.Signal(), in.Signal())
// 		// e.Release()

// 		e = sox.CreateEffect(sox.FindEffect("output"))
// 		e.Options(out)
// 		chain.Add(e, in.Signal(), in.Signal())
// 		e.Release()

// 		// The last effect in the effect chain must be something that only consumes
// 		// samples; in this case, we use the built-in handler that outputs data.
// 		// e = sox.CreateEffect(sox.FindEffect("remix"))
// 		// e.Options("-")
// 		// chain.Add(e, in.Signal(), in.Signal())
// 		// e.Release()

// 		// Flow samples through the effects processing chain until EOF is reached.
// 		// chain.Flow()
// 		chain.FlowCallback(playCallback)
// 		stoppedChan <- true
// 	}()

// 	return stoppedChan, progressChan
// }

func (m *Mixer) PlayMixed(stop chan bool) (chan bool, chan string) {
	return m.PlayFile(stop, MixedFile)
}

func (m *Mixer) PlayRecPlayback(stop chan bool) (chan bool, chan string) {
	if !m.preparePlayback() {
		return nil, nil
	}
	return m.PlayFile(stop, RecPlaybackFile)
}

func (m *Mixer) PlayFile(stop chan bool, filename string) (chan bool, chan string) {
	if !FileExists(filename) {
		panic("Nothing to play. This shouldnt happen")
	}

	stoppedChan := make(chan bool)
	progressChan := make(chan string)

	go func() {

		if !sox.Init() {
			log.Fatal("Failed to initialize SoX")
		}
		defer sox.Quit()

		// signalInfo := sox.NewSignalInfo(float64(SampleRate), uint(Channels), uint(Precision), 0, nil)
		// encoding := sox.NewEncodingInfo(SoxEncodingSign2, uint(Precision), 0, false)
		in := sox.OpenRead(filename)
		if in == nil {
			log.Fatal("Failed to open memory buffer for reading")
		}
		defer in.Release()

		// Open the output device: Specify the output signal characteristics.
		// Since we are using only simple effects, they are the same as the
		// input file characteristics.
		// Using "alsa" or "pulseaudio" should work for most files on Linux.
		// On other systems, other devices have to be used.
		out := sox.OpenWrite("default", in.Signal(), nil, "coreaudio")
		if out == nil {
			log.Fatal("Failed to open output device")
		}
		// Close the output device before exiting
		defer out.Release()

		// Create an effects chain: Some effects need to know about the
		// input or output encoding so we provide that information here.
		chain := sox.CreateEffectsChain(in.Encoding(), out.Encoding())
		// Make sure to clean up!
		defer chain.Release()

		// The first effect in the effect chain must be something that can
		// source samples; in this case, we use the built-in handler that
		// inputs data from an audio file.
		e := sox.CreateEffect(sox.FindEffect("input"))
		e.Options(in)
		// This becomes the first "effect" in the chain
		chain.Add(e, in.Signal(), in.Signal())
		e.Release()

		// The last effect in the effect chain must be something that only consumes
		// samples; in this case, we use the built-in handler that outputs data.
		e = sox.CreateEffect(sox.FindEffect("output"))
		e.Options(out)
		chain.Add(e, in.Signal(), in.Signal())
		e.Release()

		// Flow samples through the effects processing chain until EOF is reached.
		// chain.Flow()
		chain.FlowCallback(playCallback)
		stoppedChan <- true

	}()

	return stoppedChan, progressChan
}

func (m *Mixer) PlayArmed(stop chan bool) (chan bool, chan string) {
	if m.ArmedTrack == nil {
		panic("Not armed track. This shouldnt happen")
	}

	stoppedChan := make(chan bool)
	progressChan := make(chan string)

	go func() {

		if !sox.Init() {
			log.Fatal("Failed to initialize SoX")
		}
		defer sox.Quit()

		signalInfo := sox.NewSignalInfo(float64(SampleRate), uint(Channels), uint(Precision), 0, nil)
		encoding := sox.NewEncodingInfo(SoxEncodingSign2, uint(Precision), 0, false)
		in := sox.OpenMemRead0(m.ArmedTrack.Samples, signalInfo, encoding, "raw")
		if in == nil {
			log.Fatal("Failed to open memory buffer for reading")
		}

		// Open the output device: Specify the output signal characteristics.
		// Since we are using only simple effects, they are the same as the
		// input file characteristics.
		// Using "alsa" or "pulseaudio" should work for most files on Linux.
		// On other systems, other devices have to be used.
		out := sox.OpenWrite("default", in.Signal(), nil, "coreaudio")
		if out == nil {
			log.Fatal("Failed to open output device")
		}
		// Close the output device before exiting
		defer out.Release()

		// Create an effects chain: Some effects need to know about the
		// input or output encoding so we provide that information here.
		chain := sox.CreateEffectsChain(in.Encoding(), out.Encoding())
		// Make sure to clean up!
		defer chain.Release()

		// The first effect in the effect chain must be something that can
		// source samples; in this case, we use the built-in handler that
		// inputs data from an audio file.
		e := sox.CreateEffect(sox.FindEffect("input"))
		e.Options(in)
		// This becomes the first "effect" in the chain
		chain.Add(e, in.Signal(), in.Signal())
		e.Release()

		// The last effect in the effect chain must be something that only consumes
		// samples; in this case, we use the built-in handler that outputs data.
		e = sox.CreateEffect(sox.FindEffect("output"))
		e.Options(out)
		chain.Add(e, in.Signal(), in.Signal())
		e.Release()

		// Flow samples through the effects processing chain until EOF is reached.
		// chain.Flow()
		chain.FlowCallback(playCallback)
		stoppedChan <- true

	}()

	return stoppedChan, progressChan
}

func (m *Mixer) Play(stop chan bool) (chan bool, chan string) {

	if m.ArmedTrack == nil {
		panic("Not armed track. This shouldnt happen")
	}

	stoppedChan := make(chan bool)
	progressChan := make(chan string)

	go func() {
		ctx, err := malgo.InitContext(nil, malgo.ContextConfig{}, func(message string) {
			fmt.Printf("LOG <%v>\n", message)
		})
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		defer func() {
			_ = ctx.Uninit()
			ctx.Free()
		}()

		deviceConfig := malgo.DefaultDeviceConfig()
		deviceConfig.Format = malgo.FormatS32
		deviceConfig.Channels = Channels
		deviceConfig.SampleRate = SampleRate
		deviceConfig.Alsa.NoMMap = 1

		sizeInBytes := uint32(malgo.SampleSizeInBytes(deviceConfig.Format))

		var playbackSampleCount uint32

		onSendFrames := func(framecount uint32, pSamples []byte) uint32 {

			// fmt.Println("framecount", framecount)
			if framecount == 0 {
				// stop <- true
				return 0
			}

			samplesToRead := framecount * deviceConfig.Channels * sizeInBytes
			if samplesToRead > m.ArmedTrack.SamplesCount-playbackSampleCount {
				samplesToRead = m.ArmedTrack.SamplesCount - playbackSampleCount
			}

			copy(pSamples, m.ArmedTrack.Samples[playbackSampleCount:playbackSampleCount+samplesToRead])

			playbackSampleCount += samplesToRead

			progressChan <- fmt.Sprintf("%d of %d", playbackSampleCount, m.ArmedTrack.SamplesCount)
			// progressChan <- m.ArmedTrackSamplesCount

			if playbackSampleCount == m.ArmedTrack.SamplesCount {
				stop <- true
				return 0
			}

			return samplesToRead / deviceConfig.Channels / sizeInBytes
		}

		// fmt.Println("Playing...")
		playbackCallbacks := malgo.DeviceCallbacks{
			Send: onSendFrames,
		}

		device, err := malgo.InitDevice(ctx.Context, malgo.Playback, nil, deviceConfig, playbackCallbacks)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		defer device.Uninit()

		err = device.Start()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		for {
			select {
			case <-stop:
				// fmt.Println("stopped")
				stoppedChan <- true
				return
			}
		}

	}()

	return stoppedChan, progressChan
}
