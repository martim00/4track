#include "widgets/sdl-widgets.h"
#include "track.h"
#include "audio.h"
#include "track_widget.h"
#include "mixer2.h"
#include <iostream>

#include "file_dialog.h"

#if _WIN32
#define DEFAULT_PATH "C:\\"
#else
#define DEFAULT_PATH "/tmp"
#endif



TopWin *top_win;

const int wav_pnt_max = 20; // points in waveforms
const int
    bg_w = 128, // wave background width
    bg_h = 50;  // wave background height/2

const int window_width = 800;
const int window_height = 600;

template <class T, const int dim>
struct Array
{ // can insert and remove
    int end;
    T buf[dim];
    Array() : end(-1)
    {
    }
    T &operator[](int ind)
    {
        if (ind >= 0 && ind < dim)
        {
            if (end < ind)
                end = ind;
            return buf[ind];
        }
        // alert("Array: index=%d (>=%d) end=%d",ind,dim,end); if (debug) abort();
        return buf[0];
    }
    /*
  T* operator+(int ind) {  // pointer arithmetic
    if (ind>=0 && ind<dim) {
      if (end<ind) end=ind;
      return buf+ind;
    }
    alert("Array: index=%d (>=%d)",ind,dim); if (debug) abort();
    return buf;
  }
*/
    void insert(int ind, T item)
    {
        if (end >= dim - 1)
        {
            //   alert("Array insert: end >= %d-1",dim); if (debug) abort();
            return;
        }
        ++end;
        for (int j = end; j > ind; --j)
            buf[j] = buf[j - 1];
        buf[ind] = item;
    }
    void remove(int ind)
    {
        if (end < 0)
            return;
        for (int j = ind; j < end; ++j)
            buf[j] = buf[j + 1];
        --end;
    }
};

// struct WavF
// { // waveform buffer
//     short wform[bg_w];
//     Array<Point, 10000> ptbuf;
//     WavF()
//     {
//         ptbuf[0].set(0, 0);
//         ptbuf[1].set(bg_w / 4, -400);
//         ptbuf[2].set(bg_w * 3 / 4, 400);
//         ptbuf[3].set(bg_w + 1, 0); // this is the guard
//         ptbuf.end = 3;
//     }
// };

namespace we
{ // wave edit
    int state;
    int lst_x, lst_y;
    // WavF *wf;
    int act_pt;
    bool do_point(int x, int y, int but);
    int y2pt(int y) { return (y - bg_h) * 20; }
    int pt2y(int y) { return y / 20 + bg_h; }
} // namespace we

// SDL_Thread *audio_thread;

// namespace we {  // wave edit
//   int state;
//   int lst_x,lst_y;
//   WavF *wf;
//   int act_pt;
//   bool do_point(int x,int y,int but);
//   int y2pt(int y) { return (y-bg_h)*20; }
//   int pt2y(int y) { return y/20 + bg_h; }
// }

void make_spline(
    int pt_end, // dim of xs (points x buffer)
    int wdim,   // dim of ys (point val buffer)
    Point *pnt,
    short *out // waveform buffer
)
{
    int i, j,
        i0, i2, i3,
        delta;
    float x, xi, xi0, xi2, xi3;
    for (i = 0; i < pt_end; ++i)
    { // last point not used
        i0 = i == 0 ? pt_end - 1 : i - 1;
        i2 = i == pt_end - 1 ? 0 : i + 1;
        i3 = i2 == pt_end - 1 ? 0 : i2 + 1;
        delta = pnt[i2].x - pnt[i].x;
        if (delta < 0)
            delta += wdim;
        xi0 = pnt[i0].y;
        xi = pnt[i].y;
        xi2 = pnt[i2].y;
        xi3 = pnt[i3].y;
        for (j = 0; j < delta; ++j)
        {
            x = float(j) / delta;
            // From: www.musicdsp.org/showone.php?id=49
            out[pnt[i].x + j] = int(
                ((((3 * (xi - xi2) - xi0 + xi3) / 2 * x) +
                  2 * xi2 + xi0 - (5 * xi + xi3) / 2) *
                     x +
                 (xi2 - xi0) / 2) *
                    x +
                xi);
        }
    }
}

void draw_wform(BgrWin *bgw, WaveForm *waveform)
{ // supposed: bgw->parent = top_window
    bgw->clear();
    int height = bgw->tw_area.h;
    int half_height = height / 2;

    for (int i = 0; i < waveform->samples; ++i)
    {
        // int x = waveform->ptbuf[i].x,
        //     val = wf->ptbuf[i].y;
        
        int x = (i / (float)waveform->samples) * window_width;
        int yLeft = (waveform->leftChannel[i] * half_height) + half_height;
        circleColor(bgw->render, x, yLeft, 1, 0xff00ff9933);
        int yRight = (waveform->rightChannel[i] * half_height) + half_height;
        circleColor(bgw->render, x, yRight, 1, 0xff);

        // if (i == 0)
        //     circleColor(bgw->render, bg_w - 1, we::pt2y(val), 2, 0xff);
    }
    // int val2 = we::pt2y(wf->wform[127]);
    // for (i = 0; i < bg_w; ++i)
    // {
    //     int val1 = val2;
    //     val2 = we::pt2y(wf->wform[i]);
    //     SDL_Log("val %f", wf->wform[i]);
    //     lineColor(bgw->render, i, val1, i + 1, val2, 0xff);
    // }
    bgw->blit_upd();
}

void bgr_clear(BgrWin *bgr) { bgr->clear(); }

void draw_bgw1(BgrWin *wb)
{
    // make_spline(wavf1.ptbuf.end, bg_w, wavf1.ptbuf.buf, wavf1.wform);
    // draw_wform(wb,&wavf1);
    wb->clear();
    lineColor(wb->render, 10, 10, 100, 100, 0xff);
    wb->blit_upd();
}

void down(BgrWin *bgw, int x, int y, int but)
{
    // wf= bgw==bgw1 ? &wavf1 : &wavf2;
    // state=eIdle;
    // if (do_point(x,y,but)) {
    //   make_spline(wf->ptbuf.end,bg_w,wf->ptbuf.buf,wf->wform);
    //   draw_wform(bgw,wf);
    // }
}

void move(BgrWin *bgw, int x, int y, int but)
{
    // x=minmax(0,x,bg_w-2); // -2: less then last point (is first point)
    // y=minmax(0,y,bg_h*2-1);
    // if (act_pt==0) x=0;
    // if (state!=eMoving || hypot(x-lst_x,y-lst_y)<2) return;
    // lst_x=x; lst_y=y;
    // Point *pt=&wf->ptbuf[act_pt];
    // if ((act_pt>0 && pt[-1].x>=x) || pt[1].x<=x)
    //   return;
    // pt->set(x,y2pt(y));
    // make_spline(wf->ptbuf.end,bg_w,wf->ptbuf.buf,wf->wform);
    // draw_wform(bgw,wf);
}

void up(BgrWin *bgw, int x, int y, int but)
{
    // state=eIdle;
    // SDL_EventState(SDL_MOUSEMOTION,SDL_DISABLE);
}


Mixer *mixer;
std::vector<TrackWidget*> widgets;

int main(int, char **)
{
    // Set verbosity to true
    pfd::settings::verbose(true);

    // Notification
    pfd::notify("Important Notification",
                "This is ' a message, pay \" attention \\ to it!",
                pfd::icon::info);

    // Message box with nice message
    auto m = pfd::message("Personal Message",
                          "You are an amazing person, don’t let anyone make you think otherwise.",
                          pfd::choice::yes_no_cancel,
                          pfd::icon::warning);


    auto dir = pfd::select_folder("Select any directory", DEFAULT_PATH).result();
    std::cout << "Selected dir: " << dir << "\n";

    // File open
    auto f = pfd::open_file("Choose files to read", DEFAULT_PATH,
                            { "Text Files (.txt .text)", "*.txt *.text",
                              "All Files", "*" },
                            pfd::opt::multiselect);
    std::cout << "Selected files:";
    for (auto const &name : f.result())
        std::cout << " " + name;
    std::cout << "\n";

    mixer = new Mixer();

    top_win = new TopWin("4Track", Rect(300, 300, 800, 600), 0, 0, false,
                         []() {
                             top_win->clear();
                             //   draw_title_ttf->draw_string(top_win->render,"Hello world!",Point(20,40));
                         });
    Button *butLoad = new Button(top_win, 0, Rect(5, 10, 60, 0), "Load",
                                 [](Button *b) {
                                    //  mixer->load("sample.wav");
                                    Track *track = mixer->addTrack("sample.wav");
                                    widgets.push_back(new TrackWidget(*track));
                                    top_win->draw_blit_recur();
                                 });
    Button *butPlay = new Button(top_win, 0, Rect(70, 10, 60, 0), "Play",
                                 [](Button *b) {
                                     mixer->play();
                                 });

    Button *butStop = new Button(top_win, 0, Rect(140, 10, 60, 0), "Stop",
                                 [](Button *b) {
                                     mixer->stop();
                                 });

    BgrWin *bgw1 = new BgrWin(
        top_win, Rect(10, 40, 750, 50), "Wave", [](BgrWin *wb) {
            wb->clear();
            // lineColor(wb->render, 10, 10, 200, 100, 0xff);
            // RawAudio *audio = mixer->get_audio();
            // if (!audio) return;
            // WaveForm *wave = mixer->build_wave(audio, window_width);
            // make_spline(wave->ptbuf.end, bg_w, wave->ptbuf.buf, wave->wform);
            // draw_wform(wb, wave);

            wb->blit_upd();
        },
        down, move, up, cWhite);

    bgw1->show();

    // mixer->addTrack("sample.wav");

    BgrWin *waves_ctr = new BgrWin(
        top_win, Rect(10, window_height - 200, window_width - 20, 190),
        0, bgr_clear, 0, 0, 0, cForeground);
    waves_ctr->show();

    // Track *track1 = new Track(top_win, 20, window_height - 190, window_width - 40, 20);
    // Track *track2 = new Track(top_win, 20, window_height - 160, window_width - 40, 20);
    // Track *track3 = new Track(top_win, 20, window_height - 130, window_width - 40, 20);

    get_events();

    delete mixer;
    return 0;
}
