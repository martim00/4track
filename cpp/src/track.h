#ifndef TRACK_H
#define TRACK_H


#include <string>

// struct TopWin;
class WaveForm;
class RawAudio;

class Track {

public:
    // Track(TopWin *win, int x, int y, int width, int height);
    Track(const std::string &filename);
    ~Track();

    void backward();

private:
    RawAudio *audio;
    const std::string &filename;

};

#endif 