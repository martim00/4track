#ifndef TRACK_WIDGET_H
#define TRACK_WIDGET_H

class Track;

class TrackWidget {
public:
    TrackWidget(const Track& track) : track(track) {

    }

private:
    const Track& track;

};

#endif