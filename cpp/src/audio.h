#ifndef AUDIO_H
#define AUDIO_H

#include <sndfile.h>

class WaveForm
{
public:
    WaveForm(int channels, int samples) 
        : channels(channels), samples(samples)
    {
        leftChannel = new float[samples];
        if (channels == 2) {
            rightChannel = new float[samples];
        }

    }
    ~WaveForm() {
        delete [] leftChannel;
        if (channels == 2)
            delete [] rightChannel;
    }

// private:
    float *leftChannel;
    float *rightChannel;
    int channels;
    int samples;
};

class RawAudio 
{
public:
    SNDFILE *file;
    SF_INFO info;
    WaveForm *wave;

    ~RawAudio();
};

#endif // AUDIO_H