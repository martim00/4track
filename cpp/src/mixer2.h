#ifndef MIXER_H
#define MIXER_H

#include <string>
#include <sndfile.h>

#include <vector>

class Track;

class RtAudio;
class RawAudio;
class WaveForm;


class Mixer {
    
public:
    Mixer();
    ~Mixer(); 

    void play();
    // void load(const std::string &wave);
    // void close_file();
    void reset();
    void stop();
    void backward();
    RawAudio *get_audio() { return current_audio; }
    WaveForm *build_wave(RawAudio *audio, int width);
    Track* addTrack(const std::string &filename);

private:
    RawAudio *current_audio;
    RtAudio *dac;
    std::vector<Track*> tracks;
};


#endif 