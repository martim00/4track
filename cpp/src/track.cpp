#include "track.h"

#include <iostream>

#include "widgets/sdl-widgets.h"
#include "audio.h"

// namespace {
//     void bgr_clear(BgrWin *bgr) { bgr->clear(); }
// }

// Track::Track(TopWin *win, int x, int y, int width, int height) {
//     BgrWin *ui = new BgrWin(
//         win, Rect(x, y, width, height), 0, bgr_clear, 0, 0, 0, cGrey);
//     ui->show();
// }

namespace
{

    void buildWave(RawAudio *audio, int width)
    {
        SF_INFO info = audio->info;
        SNDFILE *file = audio->file;

        const float frames_per_bin = info.frames / (float)width;
        const long max_frames_per_bin = 1 + ceilf(frames_per_bin);
        long frames_per_buf, buffer_len;

        if (info.channels > 2)
        {
            std::cerr << "More than 2 channels is not allowed";
            return;
        }

        float *data = (float *)malloc(sizeof(float) * max_frames_per_bin * info.channels);
        sf_seek(file, 0, SEEK_SET);

        if (!data)
        {
            printf("out of memory.\n");
            return;
        };

        audio->wave = new WaveForm(info.channels, info.frames);

        frames_per_buf = floorf(frames_per_bin);
        buffer_len = frames_per_buf * info.channels;
        int total_frame = 0;

        while ((sf_read_float(file, data, buffer_len)) > 0)
        {
            float min, max, rms;

            for (int frame = 0; frame < frames_per_buf; frame++)
            {
                for (int ch = 0; ch < info.channels; ch++)
                {
                    if (frame * info.channels + ch > buffer_len)
                    {
                        fprintf(stderr, "index error!\n");
                        break;
                    };
                    {
                        const float sample_val = data[frame * info.channels + ch];
                        if (ch == 0)
                            audio->wave->leftChannel[total_frame] = sample_val;
                        else if (ch == 1)
                            audio->wave->rightChannel[total_frame++] = sample_val;
                    };
                };
            };
        }
    }

    RawAudio *loadFile(const std::string &filename)
    {
        SNDFILE *infile = NULL;
        SF_INFO in_sfinfo;
        // memset (&in_sfinfo, 0, sizeof (in_sfinfo)) ;

        if ((infile = sf_open(filename.c_str(), SFM_READ, &in_sfinfo)) == NULL)
        {
            std::cout << sf_error_number(sf_error(NULL));
            std::cout << "Not able to open input file %s.\n", filename;
            // puts (sf_strerror (NULL)) ;
            return NULL;
        };
        RawAudio *audio = new RawAudio();
        audio->file = infile;
        audio->info = in_sfinfo;

        // TODO: see the width here
        buildWave(audio, 500);

        return audio;
    }
} // namespace

Track::Track(const std::string &filename)
    : filename(filename)
{
    this->audio = loadFile(filename);
}

Track::~Track()
{
}

void Track::backward()
{
    sf_seek(this->audio->file, 0, SEEK_SET);
}
