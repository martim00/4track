
#ifndef TOOLS_H
#define TOOLS_H

#define OPEN_TOOL_ID 1
#define CLOSE_TOOL_ID 2
#define SETTINGS_TOOL_ID 3
#define FORWARD_TOOL_ID 4
#define BACKWARD_TOOL_ID 5
#define STOP_TOOL_ID 6
#define PAUSE_TOOL_ID 7
#define PLAY_TOOL_ID 8
#define RECORD_TOOL_ID 9
#define SAVE_TOOL_ID 10

#include "app_model.h"

void open_tool(app_model_t *app);
void save_tool(app_model_t *app);

void forward_tool(app_model_t *app);
void backward_tool(app_model_t *app);
void stop_tool(app_model_t *app);
void pause_tool(app_model_t *app);
void play_tool(app_model_t *app);
void record_tool(app_model_t *app);

#endif // TOOLS_H