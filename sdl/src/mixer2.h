#ifndef MIXER2_H
#define MIXER2_H

#include <stdbool.h>
#include <stdint.h>
#include "eq.h"


#include "audio.h"

typedef struct {
    audio_buffer_t *audio;
    float volume;
    float pan;
    EQSTATE eq;

} channel_t;

typedef struct {
    int buffer_byte_max_position;
    int buffer_byte_position;
    audio_buffer_t *recording_buffer;

    int bytes_per_second;

    channel_t **channels;
    int channels_count;

    int current_channel_rec;

    int state;
    
} mixer_t;

mixer_t *get_mixer();

bool mixer_init();
void mixer_clean();
void mixer_start_recording();
void mixer_start_playing();
bool mixer_update_recording();
bool mixer_update_playback();
void mixer_return_to_zero();

void mixer_stop();
void mixer_pause();

void mixer_change_volume(int channel, float volume);
float mixer_get_volume(int channel);

void mixer_change_pan(int channel, float pan);
float mixer_get_pan(int channel);

void mixer_change_eq_low(int channel, float gain);
void mixer_change_eq_mid(int channel, float gain);
void mixer_change_eq_high(int channel, float gain);

float mixer_get_eq_low(int channel);
float mixer_get_eq_mid(int channel);
float mixer_get_eq_high(int channel);

void mixer_set_audio_channel(int channel, int byte_size, uint8_t *audio_buffer);

#endif 
