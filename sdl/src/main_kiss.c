#include "kiss/kiss_sdl.h"
#include "app.h"


app_t app;


void setup() {

    // TODO: refactor to init all objects here
	// initialize_mixer();

    app_init(&app);

}

void update() {
    app_update(&app);
}

void process_input() {
    app_process_input(&app);
}

void render() {
    app_render(&app);
}

int main(int argc, char **argv)
{
    SDL_Log("initing...");

    setup();

	while (!app.quit) {
        // SDL_Log("running...");

		/* Some code may be written here */

		SDL_Delay(10);

        process_input();
        update();
        render();

	}
    app_clean(&app);
	return 0;
}

