#ifndef MIXER_H
#define MIXER_H

#include <stdbool.h>
// #include <SDL2_mixer/SDL_mixer.h>

typedef struct {
    short *data;
    int len;
    void *chunk;
} raw_audio_t;

bool initialize_mixer();
void destroy_mixer(void);
bool play_wave(raw_audio_t *wave);
// bool pause_wave(raw_audio_t *wave);

// raw_audio_t* load_waveform(char *filename);

extern raw_audio_t current_stream;
// extern raw_audio_t *loaded_wave;

extern float player_cursor;
extern int total_samples_played;


#endif // MIXER_H