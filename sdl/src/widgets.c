#include "widgets.h"


void control_button_new(
    control_button_gui *button, 
    char *image, 
    char *image_pressed, 
    kiss_array *objects, 
    kiss_window *window,
    int x,
    int y,
    SDL_Renderer *renderer
) 
{
    kiss_image_new(&button->button_image, image, objects, renderer);
    kiss_image_new(&button->button_pressed_image, image_pressed, objects, renderer);

	kiss_button_new(&button->button, window, "", x, y, button->button_image, button->button_pressed_image, button->button_pressed_image);
}

int control_button_draw(control_button_gui *button, SDL_Renderer *renderer)
{
	return kiss_button_draw(&button->button, renderer);
}