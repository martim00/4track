#ifndef SERIALIZER_H
#define SERIALIZER_H

#include "mixer2.h"

void serializer_save_file(mixer_t *mixer, const char *filename);
void serializer_open_file(mixer_t *mixer, const char *filename);

#endif // SERIALIZER_H

