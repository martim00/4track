#include "mixer.h"
#include <stdbool.h>

#include <SDL2/SDL.h>
// #include <SDL2_Mixer/SDL_Mixer.h>
#include "app.h"


raw_audio_t current_stream = {
    .data = NULL,
    .len = 0
};

// raw_audio_t *loaded_wave = NULL;

float player_cursor = 0;
int total_samples_played = 0;
// app_t *app = NULL;

bool initialize_mixer() {

    // if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1) {
    //   	fprintf(stderr, "Error initializing SDL Mixer\n");
    //   	return false;
	// }

    // printf("%d", MIX_DEFAULT_FORMAT);
    // // printf("%lu", sizeof(short));

	// SDL_version compile_version;
	// const SDL_version *link_version=Mix_Linked_Version();
	// SDL_MIXER_VERSION(&compile_version);
	// printf("compiled with SDL_mixer version: %d.%d.%d\n", 
    //     compile_version.major,
    //     compile_version.minor,
    //     compile_version.patch);
	// printf("running with SDL_mixer version: %d.%d.%d\n", 
    //     link_version->major,
    //     link_version->minor,
    //     link_version->patch);

    return true;
}

void destroy_mixer(void) {
    // Mix_CloseAudio();
}

// a simple channel_finished function
void channelDone(int channel) {
    printf("channel %d finished playback.\n",channel);
}

void HeadShadowEffect(int chan, void* stream, int len, void* udata) {
    // short* p = (short*) stream;    /* size= len/2 beause we use 2 bytes values, len is the size of stream in bytes */    
    // int length = len/2;
}

void noEffect(int chan, void* stream, int len, void* udata) {    
    short* p = (short*) stream;    
    int length = len/2;        /* Iterate over p 2 by 2 */    
    current_stream.data = p;
    current_stream.len = length;

    total_samples_played += length;
    // player_cursor = (float)total_samples_played / global_app->loaded_wave->len;

    // SDL_Log("cursor %f", player_cursor);



    // for (int i=0; i < length/2; i++) {        
    //     p[i*2]=0;        // left
    //     p[i*2+1]=(short)(p[i*2+1]*value);     // right
    // }
}

// make a passthru processor function that does nothing...
void noEffect2(int chan, void *stream, int len, void *udata)
{

    // you could work with stream here...
    // SDL_Log("channel %d len %d", chan, len);
    // SDL_Log("noeffect ");
    // short *raw = (short*)stream;
    // int length = len/2;

    // for (int i = 0; i < length; i++) {
    //     int data = raw[i];
    //     SDL_Log("%d", data);
    // }
    // raw_stream = stream;
}

// bool pause_wave(raw_audio_t *wave) {
//     int number = Mix_Playing(-1);
//     SDL_Log("playing %d", number);

//     int paused = Mix_Paused(-1);
//     SDL_Log("paused %d", paused);
//     if (paused > 0) {
//         Mix_Resume(-1);
//         SDL_Log("Resuming..");
//     } else {
//         Mix_Pause(-1);
//         SDL_Log("Pausing..");
//     }
//     return true;
// }

bool play_wave(raw_audio_t *wave) {
    // load sample.wav in to sample
    // Mix_Chunk *sample;
    // sample = Mix_LoadWAV(filename);

    // if (!sample) {
    //     printf("Mix_LoadWAV: %s\n", Mix_GetError());
    //     return false;
    // }


    // if (Mix_PlayChannel(1, wave->chunk, 0) == -1) {
    //     printf("Mix_PlayChannel: %s\n", Mix_GetError());
	// 	return false;
    // }

    // Mix_ChannelFinished(channelDone);
    // SDL_Delay(1000);

    // if (Mix_PlayChannel(2, sample, 1) == -1) {
    //     printf("Mix_PlayChannel: %s\n", Mix_GetError());
	// 	return false;
    // }

    // Mix_FadeOutChannel(1, 2000);


    // Mix_Volume(1, MIX_MAX_VOLUME / 2);

    // pan channel 1 halfway to the left
    // if(!Mix_SetPanning(1, 255, 100)) {
    //     printf("Mix_SetPanning: %s\n", Mix_GetError());
    //     // no panning, is it ok?
    // }

    	
    // distance channel 1 to be farthest away
    // if(!Mix_SetDistance(1, 200)) {
    //     printf("Mix_SetDistance: %s\n", Mix_GetError());
    //     // no distance, is it ok?
    // }

    // set channel 2 to be behind and right, and 100 units away
    // if(!Mix_SetPosition(1, 135, 100)) {
    //     printf("Mix_SetPosition: %s\n", Mix_GetError());
    //     // no position effect, is it ok?
    // }

    	
    // register noEffect as a postmix processor
    // if(!Mix_RegisterEffect(MIX_CHANNEL_POST, noEffect, NULL, NULL)) {
    // if (!Mix_RegisterEffect(1, noEffect, NULL, NULL)) {
    //     printf("Mix_RegisterEffect: %s\n", Mix_GetError());
    // }

    return true;
}

// raw_audio_t* load_waveform(char *filename) {

//     Mix_Chunk *sample = Mix_LoadWAV(filename);
//     if (!sample) {
//         printf("Mix_LoadWAV: %s\n", Mix_GetError());
//         return NULL;
//     }

//     Uint8 *buf = sample->abuf;

//     short* raw = (short*) buf;    
//     int length = sample->alen/2;        /* Iterate over p 2 by 2 */    

//     raw_audio_t *wave = malloc(sizeof(raw_audio_t));
//     wave->data = raw;
//     wave->len = length;
//     wave->chunk = sample;
//     return wave;
// }

// raw_audio_t* load_waveform(char *filename) {

//     SDL_AudioSpec wav_spec;
//     Uint32 wav_length;
//     Uint8 *wav_buffer;

//     /* Load the WAV */
//     if (SDL_LoadWAV(filename, &wav_spec, &wav_buffer, &wav_length) == NULL) {
//         fprintf(stderr, "Could not open wav: %s\n", SDL_GetError());
//         return NULL;
//     } else {
//         /* Do stuff with the WAV data, and then... */
//         return wav_buffer;

//         // SDL_FreeWAV(wav_buffer);
//     }

// }

