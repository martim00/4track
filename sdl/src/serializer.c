#include "serializer.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "utils.h"
#include "tinywav/tinywav.h"

#define INI_IMPLEMENTATION
#include "ini/ini.h"

#include <SDL2/SDL.h>

const int SAMPLE_RATE = 44100;
const int BIT_DEPTH = 32;
const int NUM_CHANNELS = 2;
const int BLOCK_SIZE = 480;

void save_wavefile(channel_t *channel, const char *filename)
{
    TinyWav tw;
    tinywav_open_write(&tw,
                       NUM_CHANNELS,
                       SAMPLE_RATE,
                       TW_FLOAT32,     // the output samples will be 32-bit floats. TW_INT16 is also supported
                       TW_INTERLEAVED, // the samples to be written will be assumed to be inlined in a single buffer.
                                       // Other options include TW_INTERLEAVED and TW_SPLIT
                       filename        // the output path
    );

    // fwrite(channel->audio->buffer, sizeof(char), channel->audio->buffer_byte_size, file);

    tinywav_write_f(&tw, channel->audio->buffer, channel->audio->buffer_byte_size / (NUM_CHANNELS * sizeof(float)));

    // for (int i = 0; i < channel->audio->buffer_byte_size; i++)
    // {
    // samples are always expected in float32 format,
    // regardless of file sample format
    // float samples[BLOCK_SIZE * NUM_CHANNELS];
    // tinywav_write_f(&tw, samples, sizeof(samples));
    // }

    tinywav_close_write(&tw);
}

void save_wavefile_2(channel_t *channel, const char *filename)
{
    FILE *file;
    file = fopen(filename, "wb");

    if (file == NULL)
    {
        printf("Could not open file %s", filename);
        exit(1);
    }
    // fprintf(file, "Ola mundo");

    // Header chunk
    fprintf(file, "RIFF");
    fprintf(file, "----");
    fprintf(file, "WAVE");

    // Format chunk
    fprintf(file, "fmt ");

    int size = 16;
    int compression_code = 3;
    int number_of_channels = 2;
    int sample_rate = SAMPLE_RATE;
    int byte_rate = (SAMPLE_RATE * BIT_DEPTH * number_of_channels) / 8;
    int block_align = (BIT_DEPTH * number_of_channels) / 8;
    int bit_depth = BIT_DEPTH;

    fwrite(&size, sizeof(char), 4, file);               // Size
    fwrite(&compression_code, sizeof(char), 2, file);   // Compression code
    fwrite(&number_of_channels, sizeof(char), 2, file); // Number of channels
    fwrite(&sample_rate, sizeof(char), 4, file);        // Sample rate
    fwrite(&byte_rate, sizeof(char), 4, file);          // Byte rate
    fwrite(&block_align, sizeof(char), 2, file);        // Block align
    fwrite(&bit_depth, sizeof(char), 2, file);          // Bit depth

    // writeToFile(audioFile, 16, 4); // Size
    // writeToFile(audioFile, 1, 2); // Compression code
    // writeToFile(audioFile, 1, 2); // Number of channels
    // writeToFile(audioFile, sampleRate, 4); // Sample rate
    // writeToFile(audioFile, sampleRate * bitDepth / 8, 4 ); // Byte rate
    // writeToFile(audioFile, bitDepth / 8, 2); // Block align
    // writeToFile(audioFile, bitDepth, 2); // Bit depth

    // Data chunk
    fprintf(file, "data");
    fprintf(file, "----");

    // int preAudioPosition = audioFile.tellp();
    int pre_audio_position = ftell(file);

    fwrite(channel->audio->buffer, sizeof(char), channel->audio->buffer_byte_size, file);

    // for (int i = 0; i < channel->audio->buffer_byte_size; i+=4)
    // {
    //     // this works but has distortion
    //     // uint8_t byte = channel->audio->buffer[i];
    //     // fwrite(&byte, sizeof(char), 1, file);

    //     float *source = (float*)&channel->audio->buffer[i];

    //     const double max_audioval = 3.402823466e+38F;
    //     const double min_audioval = -3.402823466e+38F;

    //     const float fmaxvolume = 1.0f / ((float)SDL_MIX_MAXVOLUME);

    //     // const float fmaxvolume = 1.0f / ((float)SDL_MIX_MAXVOLUME);
    //     // float sample = ((*source)) * fmaxvolume;
    //     float sample = ((*source));
    //     // float sample = ((*source)) * fmaxvolume;

    //     float clipped_sample = CLAMP(sample, min_audioval, max_audioval);
    //     SDL_Log("Writing sample %f, clipped %f", sample, clipped_sample);
    //     fwrite(&clipped_sample, sizeof(float), 1, file);

    //     // for (int sample = 0; sample < length; sample++)
    // }

    //  maxAmplitude = pow(2, bitDepth - 1) - 1;
    // for(int i = 0; i < sampleRate * duration; i++ ) {
    //     auto sample = sineOscillator.process();
    //     int intSample = static_cast<int> (sample * maxAmplitude);
    //     writeToFile(audioFile, intSample, 2);
    // }
    int post_audio_position = ftell(file);

    fseek(file, pre_audio_position - 4, SEEK_SET);

    int data_pos = post_audio_position - pre_audio_position;
    fwrite(&data_pos, sizeof(char), 4, file);

    // audioFile.seekp(preAudioPosition - 4);
    // writeToFile(audioFile, postAudioPosition - preAudioPosition, 4);

    fseek(file, 4, SEEK_SET);
    fwrite(&post_audio_position, sizeof(char), 4, file);

    // audioFile.seekp(4, ios::beg);
    // writeToFile(audioFile, postAudioPosition - 8, 4);

    fclose(file);
}

void create_folder(const char *folder)
{
    struct stat st = {0};

    if (stat(folder, &st) == -1)
    {
        mkdir(folder, 0700);
    }
}

void save_track_data(ini_t *ini, channel_t *channel, const char *track_name)
{
    int section = ini_section_add(ini, track_name, 0);

    char volume[10];
    sprintf(volume, "%f", channel->volume);
    ini_property_add(ini, section, "volume", 0, volume, 0);

    char pan[10];
    sprintf(pan, "%f", channel->pan);
    ini_property_add(ini, section, "pan", 0, pan, 0);

    // char pan[10];
    // sprintf(pan, "%f", channel->eq.);
    // ini_property_add(ini, section, "pan", 0, volume, 0);
}

void serializer_save_file(mixer_t *mixer, const char *folder)
{
    create_folder(folder);
    // create_ini_file(folder);

    char ini_filename[120];
    snprintf(ini_filename, 120, "%s/config.ini", folder);

    ini_t *ini = ini_create(NULL);
    ini_property_add(ini, INI_GLOBAL_SECTION, "project_name", 0, "Test", 0);

    for (int i = 0; i < mixer->channels_count; i++)
    {
        channel_t *channel = mixer->channels[i];
        if (channel->audio == NULL)
        {
            continue;
        }

        char track_name[20];
        snprintf(track_name, 20, "track_%d", i);

        char channel_filename[120];
        snprintf(channel_filename, 120, "%s/%s.wav", folder, track_name);

        save_wavefile(channel, channel_filename);

        save_track_data(ini, channel, track_name);
    }

    int size = ini_save(ini, NULL, 0); // Find the size needed
    char *data = (char *)malloc(size);
    size = ini_save(ini, data, size); // Actually save the file
    ini_destroy(ini);
    FILE *fp = fopen(ini_filename, "w");
    fwrite(data, 1, size - 1, fp);
    fclose(fp);
    free(data);
}

ini_t *load_inifile(const char *filename)
{
    FILE *fp = fopen(filename, "r");
    fseek(fp, 0, SEEK_END);
    int size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    char *data = (char *)malloc(size + 1);
    fread(data, 1, size, fp);
    data[size] = '\0';
    fclose(fp);
    ini_t *ini = ini_load(data, NULL);
    free(data);

    return ini;
}

void load_wave(const char* filename, int track_number) {
    TinyWav tw;
    tinywav_open_read(&tw,
                      filename,
                      TW_INTERLEAVED // the samples will be delivered by the read function in split format
    );

    int totalNumSamples = tw.numFramesInHeader;
    int data_size = (totalNumSamples * tw.numChannels) * sizeof(float);
    float *data = (float *)malloc(data_size);
    if (!data)
    {
        printf("Cannot initialize buffer");
        exit(-1);
    }

    int samplesRead = tinywav_read_f(&tw, data, totalNumSamples);
    if (samplesRead != totalNumSamples)
    {
        printf("Could not read all data from file %s", filename);
        exit(-1);
    }

    mixer_set_audio_channel(track_number, data_size, data); 
    tinywav_close_read(&tw);
}

void serializer_open_file(mixer_t *mixer, const char *folder)
{
    char ini_filename[120];
    snprintf(ini_filename, 120, "%s/config.ini", folder);

    ini_t *ini = load_inifile(ini_filename);

    int second_index = ini_find_property(ini, INI_GLOBAL_SECTION, "project_name", 0);
    char const *second = ini_property_value(ini, INI_GLOBAL_SECTION, second_index);
    printf("%s=%s\n", "project_name", second);

    for (int i = 0; i < 4; i++) {

        char track_name[100];
        sprintf(track_name, "track_%d", i);
        int section = ini_find_section(ini, track_name, 0);
        if (section == INI_NOT_FOUND) {
            continue;
        }

        char wavefile_name[255];
        sprintf(wavefile_name, "%s/%s.wav", folder, track_name);

        load_wave(wavefile_name, i);

        int volume_index = ini_find_property(ini, section, "volume", 0);
        char const *volume_str = ini_property_value(ini, section, volume_index);
        float volume = atof(volume_str);
        printf("%s=%f\n", "volume", volume);

        mixer_change_volume(i, volume);

        int pan_index = ini_find_property(ini, section, "pan", 0);
        char const *pan_str = ini_property_value(ini, section, pan_index);
        float pan = atof(pan_str);
        printf("%s=%f\n", "pan", pan);

        mixer_change_pan(i, pan);
    }
    // int section = ini_find_section(ini, "MySection", 0);
    // int third_index = ini_find_property(ini, section, "ThirdSetting", 0);
    // char const *third = ini_property_value(ini, section, third_index);
    // printf("%s=%s\n", "ThirdSetting", third);
    ini_destroy(ini);

    // TinyWav tw;
    // tinywav_open_read(&tw,
    //                   "/Users/bucica/workspace/4track/sdl/project/channel_0.wav",
    //                   TW_INTERLEAVED // the samples will be delivered by the read function in split format
    // );

    // int totalNumSamples = tw.numFramesInHeader;
    // // float buffer[NUM_CHANNELS * BLOCK_SIZE];
    // int data_size = (totalNumSamples * tw.numChannels) * sizeof(float);
    // float *data = (float *)malloc(data_size);
    // if (!data)
    // {
    //     printf("Cannot initialize buffer");
    //     exit(-1);
    //     // return 0;
    // }

    // int samplesRead = tinywav_read_f(&tw, data, totalNumSamples);
    // if (samplesRead != totalNumSamples)
    // {
    //     printf("Could not read all data from file %s", filename);
    //     exit(-1);
    // }

    // mixer_set_audio_channel(0, data_size, data);

    // for (int i = 0; i < 100; i++)
    // {
    //     // samples are always provided in float32 format,
    //     // regardless of file sample format
    //     float samples[NUM_CHANNELS * BLOCK_SIZE];

    //     // Split buffer requires pointers to channel buffers
    //     float *samplePtrs[NUM_CHANNELS];
    //     for (int j = 0; j < NUM_CHANNELS; ++j)
    //     {
    //         samplePtrs[j] = samples + j * BLOCK_SIZE;
    //     }

    //     tinywav_read_f(&tw, samplePtrs, BLOCK_SIZE);
    // }

    // tinywav_close_read(&tw);
}

void serializer_open_file_2(mixer_t *mixer, const char *filename)
{
    FILE *file;

    // TODO: test
    filename = "/Users/bucica/workspace/4track/sdl/project/channel_0.wav";

    if ((file = fopen(filename, "r")) == NULL)
    {
        printf("Error! opening file %s", filename);

        // Program exits if the file pointer returns NULL.
        exit(1);
    }

    char riff[4];
    char wave[4];
    char format[4];
    int file_size;

    fread(&riff, sizeof(char), 4, file);
    fread(&file_size, sizeof(char), 4, file);
    fread(&wave, sizeof(char), 4, file);
    fread(&format, sizeof(char), 4, file);

    int size;
    int compression_code;
    int number_of_channels;
    int sample_rate;
    int byte_rate;
    int block_align;
    int bit_depth;
    // int extra_size;

    fread(&size, sizeof(char), 4, file);               // Size
    fread(&compression_code, sizeof(char), 2, file);   // Compression code
    fread(&number_of_channels, sizeof(char), 2, file); // Number of channels
    fread(&sample_rate, sizeof(char), 4, file);        // Sample rate
    fread(&byte_rate, sizeof(char), 4, file);          // Byte rate
    fread(&block_align, sizeof(char), 2, file);        // Block align
    fread(&bit_depth, sizeof(char), 2, file);          // Bit depth
    // fread(&extra_size, sizeof(char), 2, file);

    // if (extra_size > 0) {
    //     fseek ( file, extra_size , SEEK_CUR );

    // }
    char data_header[4];

    // while (data_header != htonl(0x64617461)) {   // "data"
    while (strncmp(data_header, "data", 4) != 0)
    { // "data"
        fseek(file, 1, SEEK_CUR);
        fread(&data_header, sizeof(char), 4, file);
    }

    int data_size;
    // fread(&data_header, sizeof(char), 4, file);
    fread(&data_size, sizeof(char), 4, file);

    uint8_t *data = (uint8_t *)malloc(data_size * sizeof(uint8_t));
    if (!data)
    {
        printf("Cannot initialize buffer");
        exit(-1);
        // return 0;
    }

    size_t samples_read = fread(data, sizeof(uint8_t), data_size, file);
    if (samples_read != data_size)
    {
        printf("Samples read are different from data size");
        exit(-1);
    }

    // float *source = (float*)data;

    // int num_samples = data_size / number_of_channels / bit_depth / 8;

    // float *interleaved_data = (float *) malloc(num_samples*sizeof(float));

    // size_t samples_read = fread(interleaved_data, sizeof(float), tw->numChannels*len, tw->f);
    // int valid_len = (int) samples_read / tw->numChannels;

    // memcpy(data, interleaved_data, tw->numChannels*valid_len*sizeof(float));

    // for (int i = 0; i < samples_read; i+=4) {
    //     float sample = source[i];
    //     SDL_Log("Sample read: %f", sample);
    // }

    mixer_set_audio_channel(0, data_size, data);

    // free(data);

    // fprintf(file, "----");
    // fprintf(file, "WAVE");

    // Format chunk
    // fprintf(file, "fmt ");

    // size_t samples_read = fread(interleaved_data, sizeof(char), 4, file);

    //    fscanf(file,"%d", &num);

    //    printf("Value of n=%d", num);
    fclose(file);
}
