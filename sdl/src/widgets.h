#ifndef INCLUDED_WIDGETS_H
#define INCLUDED_WIDGETS_H

#include "kiss/kiss_sdl.h"

typedef void (*callback_t)(void);

typedef struct {

    kiss_slider slider;
    kiss_knob knob1;
    kiss_knob knob2;
    kiss_knob knob3;
    kiss_knob knob4;

} channel_gui;

typedef struct {
	kiss_button button;
    kiss_image button_image;
    kiss_image button_pressed_image;
    callback_t button_on_click;

} control_button_gui;

void control_button_new(
    control_button_gui *button, 
    char *image, 
    char *image_pressed, 
    kiss_array *objects, 
    kiss_window *window,
    int x,
    int y,
    SDL_Renderer *renderer
);

int control_button_draw(control_button_gui *button, SDL_Renderer *renderer);

#endif