#include "app.h"
#include "kiss/kiss_sdl.h"
#include "mixer2.h"
#include "tools.h"

app_t *global_app = NULL;

static void button_event(app_t *app, kiss_button *button, SDL_Event *e,
							  int *quit, int *draw, int tool)
{

	if (kiss_button_event(button, e, draw))
	{
		switch (tool)
		{
		case PLAY_TOOL_ID:
			play_tool(&app->app_model);
			break;
		case RECORD_TOOL_ID:
			record_tool(&app->app_model);
			break;
		case STOP_TOOL_ID:
			stop_tool(&app->app_model);
			break;
		case PAUSE_TOOL_ID:
			pause_tool(&app->app_model);
			break;
		case FORWARD_TOOL_ID:
			forward_tool(&app->app_model);
			break;
		case BACKWARD_TOOL_ID:
			backward_tool(&app->app_model);
			break;
		default:
			SDL_Log("Could not handle tool");
		}
	}
}

void refresh_widgets(app_t *app)
{
	kiss_slider_update_value(&app->channel1.slider, mixer_get_volume(0));
	kiss_slider_update_value(&app->channel2.slider, mixer_get_volume(1));
	kiss_slider_update_value(&app->channel3.slider, mixer_get_volume(2));
	kiss_slider_update_value(&app->channel4.slider, mixer_get_volume(3));

	kiss_knob_update_value(&app->channel1.knob4, mixer_get_pan(0));
	kiss_knob_update_value(&app->channel2.knob4, mixer_get_pan(1));
	kiss_knob_update_value(&app->channel3.knob4, mixer_get_pan(2));
	kiss_knob_update_value(&app->channel4.knob4, mixer_get_pan(3));

	app_render(app);
}

static void handle_menu_item(app_t *app, kiss_menuitem *item, SDL_Event *e,
							 int *quit, int *draw)
{
	switch (item->id)
	{
	case OPEN_TOOL_ID:
		open_tool(&app->app_model);
		refresh_widgets(app);
		break;
	case SAVE_TOOL_ID:
		save_tool(&app->app_model);
		break;
	case CLOSE_TOOL_ID:
		*quit = 1;
		break;
	}
}

void app_init(app_t *app)
{
	app->fps_lasttime = 0;
	app->fps_frames = 0;
	app->fps_lasttime = SDL_GetTicks(); // the last recorded time.

	app->app_model.state = STATE_PAUSED;

	if (!app_init_app(app))
	{
		exit(-1);
	}

	if (!mixer_init())
	{
		exit(-1);
	}

	if (!app_init_widgets(app))
	{
		exit(-1);
	}

	global_app = app;
}

bool app_init_app(app_t *app)
{
	app->quit = 0;
	app->draw = 1;

	kiss_decoration_color.r = 0;
	kiss_decoration_color.g = 100;
	kiss_decoration_color.b = 9;
	kiss_decoration_color.a = 255;

	app->renderer = kiss_init("4 track", &app->objects, 640, 480);
	if (!app->renderer)
		return false;

	return true;
}

bool app_init_widgets(app_t *app)
{
	// kiss_array_new(&a);
	// kiss_array_append(&app->objects, ARRAY_TYPE, &a);

	/* Arrange the widgets nicely relative to each other */
	kiss_window_new(&app->window, NULL, 0, 0, 0, kiss_screen_width,
					kiss_screen_height);

	app->window.bg.r = 10;
	app->window.bg.g = 10;
	app->window.bg.b = 10;
	app->window.bg.a = 255;

	// kiss_canvas_new(&app->canvas, &app->window, 10, 30, 100, 100);

	// kiss_label_new(&app->title, &app->window, "4 track", 10, 10);

	kiss_image_new(&app->background_img, "bg_mixer_no_menu.png", NULL, app->renderer);

	kiss_menu_new(&app->menu, &app->window, 0, 0, kiss_screen_width);
	kiss_submenu *submenu = kiss_menu_add_submenu(&app->menu, "4Track");
	kiss_menu_add_item(submenu, "Open", OPEN_TOOL_ID);
	kiss_menu_add_item(submenu, "Save", SAVE_TOOL_ID);
	kiss_menu_add_item(submenu, "Quit", CLOSE_TOOL_ID);

	submenu = kiss_menu_add_submenu(&app->menu, "Settings");
	kiss_menu_add_item(submenu, "Settings", SETTINGS_TOOL_ID);

	kiss_slider_new(&app->channel1.slider, &app->window, 108, 290, 15, 70, mixer_get_volume(0));
	kiss_slider_new(&app->channel2.slider, &app->window, 160, 290, 15, 70, mixer_get_volume(1));
	kiss_slider_new(&app->channel3.slider, &app->window, 212, 290, 15, 70, mixer_get_volume(2));
	kiss_slider_new(&app->channel4.slider, &app->window, 266, 290, 15, 70, mixer_get_volume(3));


	const int knobs_margin_x = 53;
	const int knobs_margin_y = 35;

	kiss_knob_new(&app->channel1.knob1, &app->window, 114 + knobs_margin_x * 0, 147, 10, theme_color3, theme_color2, mixer_get_eq_high(0), 0.25, 0.5);
	kiss_knob_new(&app->channel1.knob2, &app->window, 114 + knobs_margin_x * 0, 147 + knobs_margin_y * 1, 10, theme_color3, theme_color2, mixer_get_eq_mid(0), 0.25, 0.5);
	kiss_knob_new(&app->channel1.knob3, &app->window, 114 + knobs_margin_x * 0, 147 + knobs_margin_y * 2, 10, theme_color3, theme_color2, mixer_get_eq_low(0), 0.25, 0.5);
	kiss_knob_new(&app->channel1.knob4, &app->window, 114 + knobs_margin_x * 0, 147 + knobs_margin_y * 3, 10, theme_color3, theme_color2, 1 - mixer_get_pan(0), 0.25, 1);

	kiss_knob_new(&app->channel2.knob1, &app->window, 114 + knobs_margin_x * 1, 147, 10, theme_color3, theme_color2, mixer_get_eq_high(1), 0.25, 0.5);
	kiss_knob_new(&app->channel2.knob2, &app->window, 114 + knobs_margin_x * 1, 147 + knobs_margin_y * 1, 10, theme_color3, theme_color2, mixer_get_eq_mid(1), 0.25, 0.5);
	kiss_knob_new(&app->channel2.knob3, &app->window, 114 + knobs_margin_x * 1, 147 + knobs_margin_y * 2, 10, theme_color3, theme_color2, mixer_get_eq_low(1), 0.25, 0.5);
	kiss_knob_new(&app->channel2.knob4, &app->window, 114 + knobs_margin_x * 1, 147 + knobs_margin_y * 3, 10, theme_color3, theme_color2, 1 - mixer_get_pan(1), 0.25, 1);

	kiss_knob_new(&app->channel3.knob1, &app->window, 114 + knobs_margin_x * 2, 147, 10, theme_color3, theme_color2, mixer_get_eq_high(2), 0.25, 0.5);
	kiss_knob_new(&app->channel3.knob2, &app->window, 114 + knobs_margin_x * 2, 147 + knobs_margin_y * 1, 10, theme_color3, theme_color2, mixer_get_eq_mid(2), 0.25, 0.5);
	kiss_knob_new(&app->channel3.knob3, &app->window, 114 + knobs_margin_x * 2, 147 + knobs_margin_y * 2, 10, theme_color3, theme_color2, mixer_get_eq_low(2), 0.25, 0.5);
	kiss_knob_new(&app->channel3.knob4, &app->window, 114 + knobs_margin_x * 2, 147 + knobs_margin_y * 3, 10, theme_color3, theme_color2, 1 - mixer_get_pan(2), 0.25, 1);

	kiss_knob_new(&app->channel4.knob1, &app->window, 114 + knobs_margin_x * 3, 147, 10, theme_color3, theme_color2, mixer_get_eq_high(3), 0.25, 0.5);
	kiss_knob_new(&app->channel4.knob2, &app->window, 114 + knobs_margin_x * 3, 147 + knobs_margin_y * 1, 10, theme_color3, theme_color2, mixer_get_eq_mid(3), 0.25, 0.5);
	kiss_knob_new(&app->channel4.knob3, &app->window, 114 + knobs_margin_x * 3, 147 + knobs_margin_y * 2, 10, theme_color3, theme_color2, mixer_get_eq_low(3), 0.25, 0.5);
	kiss_knob_new(&app->channel4.knob4, &app->window, 114 + knobs_margin_x * 3, 147 + knobs_margin_y * 3, 10, theme_color3, theme_color2, 1 - mixer_get_pan(3), 0.25, 1);

	control_button_new(
		&app->button_record, 
		"../assets/record.png", 
		"../assets/record_pressed.png", 
		&app->objects, 
		&app->window, 
		388, 
		342, 
		app->renderer
	);

	control_button_new(
		&app->button_play, 
		"../assets/play.png", 
		"../assets/play_pressed.png", 
		&app->objects, 
		&app->window, 
		414, 
		342, 
		app->renderer
	);

	control_button_new(
		&app->button_backward, 
		"../assets/backward.png", 
		"../assets/backward_pressed.png", 
		&app->objects, 
		&app->window, 
		440, 
		342, 
		app->renderer
	);

	control_button_new(
		&app->button_forward, 
		"../assets/forward.png", 
		"../assets/forward_pressed.png", 
		&app->objects, 
		&app->window, 
		466, 
		342, 
		app->renderer
	);

	control_button_new(
		&app->button_stop, 
		"../assets/stop.png", 
		"../assets/stop_pressed.png", 
		&app->objects, 
		&app->window, 
		492, 
		342, 
		app->renderer
	);

	control_button_new(
		&app->button_pause, 
		"../assets/pause.png", 
		"../assets/pause_pressed.png", 
		&app->objects, 
		&app->window, 
		518,
		342, 
		app->renderer
	);

	/* Do that, and all widgets associated with the window will show */
	app->window.visible = 1;
	return true;
}

void app_update(app_t *app)
{
	if (app->app_model.state == STATE_PLAYING)
	{
		if (mixer_update_playback())
		{
			SDL_Log("Pausing playback");
			app->app_model.state = STATE_PAUSED;
		}
	}
	else if (app->app_model.state == STATE_RECORDING)
	{
		if (mixer_update_recording())
		{
			SDL_Log("Pausing recording");
			app->app_model.state = STATE_PAUSED;
		}
	}
}

void app_process_input(app_t *app)
{
	SDL_Event e;

	while (SDL_PollEvent(&e))
	{
		if (e.type == SDL_QUIT)
			app->quit = true;

		kiss_window_event(&app->window, &e, &app->draw);

		// buttons
		button_event(app, &app->button_record.button, &e, &app->quit, &app->draw, RECORD_TOOL_ID);
		button_event(app, &app->button_play.button, &e, &app->quit, &app->draw, PLAY_TOOL_ID);
        button_event(app, &app->button_backward.button, &e, &app->quit, &app->draw, BACKWARD_TOOL_ID);
		button_event(app, &app->button_forward.button, &e, &app->quit, &app->draw, FORWARD_TOOL_ID);
		button_event(app, &app->button_stop.button, &e, &app->quit, &app->draw, STOP_TOOL_ID);
		button_event(app, &app->button_pause.button, &e, &app->quit, &app->draw, PAUSE_TOOL_ID);

		kiss_menuitem *clicked = kiss_menu_event(&app->menu, &e, &app->draw);
		if (clicked != NULL)
			handle_menu_item(app, clicked, &e, &app->quit, &app->draw);

		if (kiss_slider_event(&app->channel1.slider, &e, &app->draw))
		{
			mixer_change_volume(0, app->channel1.slider.value);
		}
		if (kiss_slider_event(&app->channel2.slider, &e, &app->draw))
		{
			mixer_change_volume(1, app->channel2.slider.value);
		}
		if (kiss_slider_event(&app->channel3.slider, &e, &app->draw))
		{
			mixer_change_volume(2, app->channel3.slider.value);
		}
		if (kiss_slider_event(&app->channel4.slider, &e, &app->draw))
		{
			mixer_change_volume(3, app->channel4.slider.value);
		}

		// channel 1
		if (kiss_knob_event(&app->channel1.knob1, &e, &app->draw))
		{
			// channel 1 eq
			mixer_change_eq_high(0, app->channel1.knob1.user_value);
		}
		if (kiss_knob_event(&app->channel1.knob2, &e, &app->draw))
		{
			// channel 1 eq
			mixer_change_eq_mid(0, app->channel1.knob2.user_value);
		}
		if (kiss_knob_event(&app->channel1.knob3, &e, &app->draw))
		{
			// channel 1 eq
			mixer_change_eq_low(0, app->channel1.knob3.user_value);
		}
		if (kiss_knob_event(&app->channel1.knob4, &e, &app->draw))
		{
			// channel 1 pan
			mixer_change_pan(0, 1 - app->channel1.knob4.user_value);
		}

		// channel 2
		if (kiss_knob_event(&app->channel2.knob1, &e, &app->draw))
		{
			// channel 2 eq
			mixer_change_eq_high(1, app->channel2.knob1.user_value);
		}
		if (kiss_knob_event(&app->channel2.knob2, &e, &app->draw))
		{
			// channel 2 eq
			mixer_change_eq_mid(1, app->channel2.knob2.user_value);
		}
		if (kiss_knob_event(&app->channel2.knob3, &e, &app->draw))
		{
			// channel 2 eq
			mixer_change_eq_low(1, app->channel2.knob3.user_value);
		}
		if (kiss_knob_event(&app->channel2.knob4, &e, &app->draw)) 
		{
			// channel 2 pan
			mixer_change_pan(1, 1 - app->channel2.knob4.user_value);
		}

		// channel 3
		if (kiss_knob_event(&app->channel3.knob1, &e, &app->draw))
		{
			// channel 3 eq
			mixer_change_eq_high(2, app->channel3.knob1.user_value);
		}
		if (kiss_knob_event(&app->channel3.knob2, &e, &app->draw))
		{
			// channel 3 eq
			mixer_change_eq_mid(2, app->channel3.knob2.user_value);
		}
		if (kiss_knob_event(&app->channel3.knob3, &e, &app->draw))
		{
			// channel 3 eq
			mixer_change_eq_low(2, app->channel3.knob3.user_value);
		}
		if (kiss_knob_event(&app->channel3.knob4, &e, &app->draw))
		{
			// channel 3 pan
			mixer_change_pan(2, 1 - app->channel3.knob4.user_value);
		}

		if (kiss_knob_event(&app->channel4.knob1, &e, &app->draw))
		{
			// channel 4 eq
			mixer_change_eq_high(3, app->channel4.knob1.user_value);
		}
		if (kiss_knob_event(&app->channel4.knob2, &e, &app->draw))
		{
			// channel 4 eq
			mixer_change_eq_mid(3, app->channel4.knob2.user_value);
		}
		if (kiss_knob_event(&app->channel4.knob3, &e, &app->draw))
		{
			// channel 4 eq
			mixer_change_eq_low(3, app->channel4.knob3.user_value);
		}
		if (kiss_knob_event(&app->channel4.knob4, &e, &app->draw))
		{
			// channel 4 pan
			mixer_change_pan(3, 1 - app->channel4.knob4.user_value);
		}
	}
}

void app_render(app_t *app)
{
	if (!app->draw)
		return;
	// SDL_Log("rendering...");
	SDL_RenderClear(app->renderer);

	kiss_window_draw(&app->window, app->renderer);
	kiss_renderimage(app->renderer, app->background_img, 0, 0, NULL);
	kiss_menu_draw(&app->menu, app->renderer);
	kiss_slider_draw(&app->channel1.slider, app->renderer);
	kiss_slider_draw(&app->channel2.slider, app->renderer);
	kiss_slider_draw(&app->channel3.slider, app->renderer);
	kiss_slider_draw(&app->channel4.slider, app->renderer);

	kiss_knob_draw(&app->channel1.knob1, app->renderer);
	kiss_knob_draw(&app->channel1.knob2, app->renderer);
	kiss_knob_draw(&app->channel1.knob3, app->renderer);
	kiss_knob_draw(&app->channel1.knob4, app->renderer);

	kiss_knob_draw(&app->channel2.knob1, app->renderer);
	kiss_knob_draw(&app->channel2.knob2, app->renderer);
	kiss_knob_draw(&app->channel2.knob3, app->renderer);
	kiss_knob_draw(&app->channel2.knob4, app->renderer);

	kiss_knob_draw(&app->channel3.knob1, app->renderer);
	kiss_knob_draw(&app->channel3.knob2, app->renderer);
	kiss_knob_draw(&app->channel3.knob3, app->renderer);
	kiss_knob_draw(&app->channel3.knob4, app->renderer);

	kiss_knob_draw(&app->channel4.knob1, app->renderer);
	kiss_knob_draw(&app->channel4.knob2, app->renderer);
	kiss_knob_draw(&app->channel4.knob3, app->renderer);
	kiss_knob_draw(&app->channel4.knob4, app->renderer);

	control_button_draw(&app->button_record, app->renderer);
	control_button_draw(&app->button_play, app->renderer);
	control_button_draw(&app->button_backward, app->renderer);
	control_button_draw(&app->button_forward, app->renderer);
	control_button_draw(&app->button_stop, app->renderer);
	control_button_draw(&app->button_pause, app->renderer);

	SDL_RenderPresent(app->renderer);
	app->draw = 0;
}

void app_clean(app_t *app)
{
	kiss_clean(&app->objects);
	mixer_clean();
}
