#include <stdio.h>

#include "display.h"
#include "mixer.h"
#include "app.h"

SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;

SDL_Texture *color_buffer_texture = NULL;
uint32_t *color_buffer = NULL;

int window_width = 800;
int window_height = 600;


bool initialize_window(void) {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
      	fprintf(stderr, "Error initializing SDL\n");
      	return false;
   	}

	// full screen
	// SDL_DisplayMode display_mode;
	// SDL_GetCurrentDisplayMode(0, &display_mode);
	// window_width = display_mode.w;
	// window_height = display_mode.h;

   	// Create a SDL window
   	window = SDL_CreateWindow(
      		NULL, 
      		SDL_WINDOWPOS_CENTERED,
      		SDL_WINDOWPOS_CENTERED,
      		window_width,
      		window_height,
			SDL_WINDOW_OPENGL 
			// SDL_WINDOW_BORDERLESS
	);
	if (!window) {
		fprintf(stderr, "Error creating a SDL window\n");
		return false;
	}

	// Create the renderer
	renderer = SDL_CreateRenderer(window, -1, 0);
	if (!renderer) {
		fprintf(stderr, "Error creating the renderer\n");
		return false;
	}
   	return true;

}

void clear_color_buffer(uint32_t color) {
	for (int y = 0; y < window_height; y++) {
		for (int x = 0; x < window_width; x++) {
			draw_pixel(x, y, color);
		}

	}
	
}

void render_color_buffer() {
	SDL_UpdateTexture(
		color_buffer_texture,
		NULL,
		color_buffer,
		(int)(window_width * sizeof(uint32_t))
	);
	SDL_RenderCopy(renderer, color_buffer_texture, NULL, NULL);

}

void draw_pixel(uint32_t x, uint32_t y, uint32_t color) {

	if (x >= 0 && x < window_width && y >= 0 && y < window_height) {
		int alpha = (color & 0xFF000000) >> 24;
		int red = (color & 0x00FF0000) >> 16;
		int green = (color & 0x0000FF00) >> 8;
		int blue = (color & 0x000000FF) >> 0;
		// SDL_Log("%d %d %d %d", alpha, red, green, blue);
		SDL_SetRenderDrawColor(global_app->renderer, red, green, blue, alpha);
		SDL_RenderDrawPoint(global_app->renderer, x, y);
		// color_buffer[(window_width * y) + x] = color;
	}
}

void draw_grid() {
	for (int y = 0; y < window_height; y++) {
		
		for (int x = 0; x < window_width; x++) {
			if (x % 10 == 0 || y % 10 == 0)
				draw_pixel(x, y, 0xFF00FF00);
		}
	}
}

void draw_rect(uint32_t x, uint32_t y, uint32_t width, uint32_t height, uint32_t color) {
	for (int j = y; j < y + height; j++) {
		for (int i = x; i < x + width; i++) {
			draw_pixel(i, j, color);
		}
	}
}

void draw_triangle(int x0, int y0, int x1, int y1, int x2, int y2, uint32_t color) {
	draw_line(x0, y0, x1, y1, color);
	draw_line(x1, y1, x2, y2, color);
	draw_line(x2, y2, x0, y0, color);
}

void draw_line(int x0, int y0, int x1, int y1, uint32_t color) {
	int delta_x = x1 - x0;
	int delta_y = y1 - y0;

	int side_length = abs(delta_x) >= abs(delta_y) ? delta_x : delta_y;

	float x_inc = delta_x / (float) side_length;
	float y_inc = delta_y / (float) side_length;

	float current_x = x0;
	float current_y = y0;

	for (int i = 0; i <= side_length; i++) {
		draw_pixel(round(current_x), round(current_y), color);
		current_x += x_inc;
		current_y += y_inc;
	}
}

void draw_waveform(raw_audio_t *waveform) {
	int length = waveform->len / 2;

	for (int i = 0; i < length; i++) {
		float raw_value_left = (float)waveform->data[i*2] / 32768;
		float raw_value_right = (float)waveform->data[i*2+1] / 32768;
		float x = (i / (float)length) * (window_width);
		draw_pixel(x, raw_value_left * (window_height) + (window_height / 2), 0xFF0FF000);
		draw_pixel(x, raw_value_right * (window_height) + (window_height / 2), 0xFFFF00FF);
	}
}

void draw_controls() {
	int control_height = 30;
	int x_center = window_width / 2;

	int control_x = x_center - 50;
	int control_y = window_height - control_height;

	draw_rect(control_x, control_y, 100, control_height, 0xFF00bb00);
	draw_rect(control_x, control_y, 30, control_height, 0xFFbbaadd);
}

void draw_cursor(float player_cursor) {
	draw_line(player_cursor * window_width, 0, player_cursor * window_width, window_height, 0xFF000000);
}

void destroy_window(void) {
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}
