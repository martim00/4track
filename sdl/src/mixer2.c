#include "mixer2.h"
#include <assert.h>
#include "utils.h"
#include <SDL2/SDL.h>

#define MIXER_IDLE 0
#define MIXER_RECORDING 1
#define MIXER_PLAYING 2
#define MIXER_STOP 3
#define MIXER_PAUSE 4

SDL_AudioDeviceID recording_device_id = 0;
SDL_AudioDeviceID playback_device_id = 0;

SDL_AudioSpec recording_spec;
SDL_AudioSpec playback_spec;

// int buffer_byte_size = 0;
// int buffer_byte_max_position = 0;
// int buffer_byte_position = 0;

#define RECORDING_BUFFER_SECONDS 6
#define MAX_RECORDING_SECONDS 5
#define MAX_CHANNELS 4

static mixer_t mixer;

mixer_t *get_mixer()
{
    return &mixer;
}

void mixer_start_recording()
{
    // Go back to beginning of buffer
    mixer.buffer_byte_position = 0;
    // memset(mixer.recording_buffer, 0, buffer_byte_size);
    reset_audio_buffer(mixer.recording_buffer);

    // Start recording
    SDL_PauseAudioDevice(recording_device_id, SDL_FALSE);

    mixer.state = MIXER_RECORDING;

    SDL_Log("Recording...");
}

void audio_recording_callback(void *userdata, Uint8 *stream, int len)
{
    // Copy audio from stream
    memcpy(&mixer.recording_buffer->buffer[mixer.buffer_byte_position], stream, len);

    // Move along buffer
    mixer.buffer_byte_position += len;

    SDL_Log("Recording callback: %d bytes", len);
}

int get_bytes_per_sample()
{
    return recording_spec.channels * (SDL_AUDIO_BITSIZE(recording_spec.format) / 8);
}

int get_bytes_per_second()
{
    // Calculate per sample bytes
    int bytes_per_sample = get_bytes_per_sample();

    // Calculate bytes per second
    return recording_spec.freq * bytes_per_sample;
}

void mixer_assign_recording()
{
    mixer_set_audio_channel(mixer.current_channel_rec, mixer.buffer_byte_position, mixer.recording_buffer->buffer);

    mixer.current_channel_rec += 1;
    if (mixer.current_channel_rec >= MAX_CHANNELS)
    {
        mixer.current_channel_rec = 0;
    }
}

void mixer_set_audio_channel(int channel, int byte_size, uint8_t *audio_buffer)
{
    channel_t *current_channel = mixer.channels[channel];
    if (current_channel->audio != NULL)
    {
        release_audio_buffer(current_channel->audio);
        current_channel->audio = NULL;
    }

    current_channel->audio = init_audio_buffer(byte_size);
    memcpy(current_channel->audio->buffer, audio_buffer, byte_size);
}

bool mixer_update_recording()
{
    // Lock recording
    SDL_LockAudioDevice(recording_device_id);

    // Checks if should finished recording
    if (mixer.buffer_byte_position > mixer.buffer_byte_max_position)
    {
        // Stop recording audio
        SDL_PauseAudioDevice(recording_device_id, SDL_TRUE);
        SDL_UnlockAudioDevice(recording_device_id);

        mixer_assign_recording();
        mixer.buffer_byte_position = 0;

        return true;
    }

    // Unlock callback
    SDL_UnlockAudioDevice(recording_device_id);
    return false;
}

void mixer_start_playing()
{
    // Start playback
    SDL_PauseAudioDevice(playback_device_id, SDL_FALSE);

    mixer.state = MIXER_PLAYING;

    SDL_Log("Playing...");
}

void audio_playback_callback(void *userdata, Uint8 *stream, int len)
{
    // int samples_to_write = len / get_bytes_per_sample();
    // for (int sample_index = 0; sample_index < samples_to_write; sample_index++) {
    // *stream++ = mixer.recording_buffer[buffer_byte_position + sample_index]; // Left channel value
    // *stream++ = SampleValue; // Right channel value

    // }
    // int bytes_per_sample = get_bytes_per_sample();
    int current_seconds = mixer.buffer_byte_position / mixer.bytes_per_second;

    SDL_memset(stream, 0, (size_t)len);

    for (int j = 0; j < MAX_CHANNELS; j++)
    {
        channel_t *current_channel = mixer.channels[j];
        if (current_channel->audio == NULL) 
            continue;

        if (mixer.buffer_byte_position >= current_channel->audio->buffer_byte_size)
            continue;

        int volume = (int)(current_channel->volume * SDL_MIX_MAXVOLUME);

        float *dest = (float*)stream;
        float *source = (float*)&current_channel->audio->buffer[mixer.buffer_byte_position];

        const double max_audioval = 3.402823466e+38F;
        const double min_audioval = -3.402823466e+38F;

        const float fmaxvolume = 1.0f / ((float)SDL_MIX_MAXVOLUME);

        float right_volume = current_channel->pan * volume;
        float left_volume = (1 - current_channel->pan) * volume;

        SDL_Log("Mixing channel %d, volume: %d; left: %f, right: %f at %d seconds", j, volume, left_volume, right_volume, current_seconds);

        int length = len / 8; // 4 bytes per sample * 2 channels
        for (int sample = 0; sample < length; sample++) 
        {
            float left = ((*source) * left_volume) * fmaxvolume;
            source++;
            float right = ((*source) * right_volume) * fmaxvolume;
            source++;

            left = do_3band(&current_channel->eq, left);
            right = do_3band(&current_channel->eq, right);

            float dst_left = *dest + left;
            dst_left = CLAMP(dst_left, min_audioval, max_audioval);
            *(dest++) = dst_left;

            float dst_right = *dest + right;
            dst_right = CLAMP(dst_right, min_audioval, max_audioval);
            *(dest++) = dst_right;
        }

        // int volume = (int)(current_channel->volume * SDL_MIX_MAXVOLUME);
        
        // SDL_Log("Mixing channel %d, volume: %d", j, volume);

        // SDL_MixAudioFormat(
        //     stream,
        //     &current_channel->audio[buffer_byte_position],
        //     recording_spec.format, // AUDIO_F32,
        //     len,
        //     volume
        // );
    }

    // int bytes_per_sample = get_bytes_per_sample() / 2;
    // int samples_to_write = len / bytes_per_sample;
    // for (int sample_index = 0; sample_index < samples_to_write; sample_index++) {
    // // for (int i = 0; i < len; i++) {
    //     // stream[i] = mixer.recording_buffer[buffer_byte_position + i];
    //     // stream[i] = 0;

    //     for (int j = 0; j < MAX_CHANNELS; j++) {
    //         channel_t *current_channel = mixer.channels[j];
    //         if (current_channel->audio == NULL) {
    //             continue;
    //         }
    //         // SDL_Log("Mixing channel %d", j);
    //         float current = (float)current_channel->audio[buffer_byte_position + (sample_index * (bytes_per_sample))];
    //         // SDL_Log("Current sample: {%f}", current);
    //         stream[buffer_byte_position + (sample_index * (bytes_per_sample))] = current;

    //         // stream[i] = current + stream[i];
    //         // result = (src->buffer[(n    ) & BUFFER_MASK] * src->lgain) >> FX_BITS;
    //         SDL_MixAudioFormat(
    //             stream + i,
    //             current_channel->audio[buffer_byte_position + i],
    //             AUDIO_F32,
    //             1,
    //             100
    //         );
    //         // int x = (cmixer.buffer[i] * cmixer.gain) >> FX_BITS;
    //         // stream[i] = CLAMP(stream[i], -32768, 32767);

    //     }
    // }
    // for (i = 0; i < len; i++) {
    //     int x = (cmixer.buffer[i] * cmixer.gain) >> FX_BITS;
    //     dst[i] = CLAMP(x, -32768, 32767);
    // }

    // for (int i=0;i<len;i++) {
    //     // stream[i] = i;
    //     *stream++ =
    // }

    // unsigned int *mixdown =
    // SDL_MixAudioFormat

    // Copy audio to stream
    // memcpy(stream, &mixer.recording_buffer[buffer_byte_position], len);
    // memcpy(stream, &mixdown, len);

    // Move along buffer
    mixer.buffer_byte_position += len;
    SDL_Log("Playing back callback: %d bytes", len);
}

bool all_channels_have_finished() {
    for (int j = 0; j < MAX_CHANNELS; j++)
    {
        channel_t *current_channel = mixer.channels[j];
        if (current_channel->audio == NULL) 
            continue;

        // if any channel has still audio to play return false
        if (mixer.buffer_byte_position < current_channel->audio->buffer_byte_size) {
            return false;
        }
    }

    // if no more channels have audio to play return true
    return true;
}

/// @brief Updates playback and determines if should stop or not (must be called by the main loop)
/// @return true if should stop, false otherwise
bool mixer_update_playback()
{
    // Lock callback
    SDL_LockAudioDevice(playback_device_id);

    // Finished playback
    if (mixer.buffer_byte_position > mixer.buffer_byte_max_position || all_channels_have_finished())
    {
        // Stop playing audio
        SDL_PauseAudioDevice(playback_device_id, SDL_TRUE);
        SDL_UnlockAudioDevice(playback_device_id);
        mixer.buffer_byte_position = 0;
        return true;
    }

    // Unlock callback
    SDL_UnlockAudioDevice(playback_device_id);
    return false;
}

void mixer_stop_playback() 
{
    SDL_LockAudioDevice(playback_device_id);

    SDL_PauseAudioDevice(playback_device_id, SDL_TRUE);
    SDL_UnlockAudioDevice(playback_device_id);
}

void mixer_stop_record() 
{
    SDL_LockAudioDevice(recording_device_id);

    SDL_PauseAudioDevice(recording_device_id, SDL_TRUE);
    SDL_UnlockAudioDevice(recording_device_id);

    mixer_assign_recording();
}

void mixer_stop() 
{
    if (mixer.state == MIXER_PLAYING) {
        mixer_stop_playback();
    } else if (mixer.state == MIXER_RECORDING) {
        mixer_stop_record();
    }

    mixer.buffer_byte_position = 0;
    mixer.state = MIXER_STOP;
}

void mixer_pause()
{
    if (mixer.state == MIXER_PLAYING) {
        mixer_stop_playback();
    } else if (mixer.state == MIXER_RECORDING) {
        mixer_stop_record();
    }
    mixer.state = MIXER_PAUSE;
}

void mixer_return_to_zero()
{
    mixer_stop();
    mixer.buffer_byte_position = 0;
}

void mixer_change_volume(int channel, float volume)
{
    assert(channel < mixer.channels_count);
    mixer.channels[channel]->volume = volume;
}

float mixer_get_volume(int channel)
{
    assert(channel < mixer.channels_count);
    return mixer.channels[channel]->volume;
}

void mixer_change_pan(int channel, float pan)
{
    assert(channel < mixer.channels_count);
    mixer.channels[channel]->pan = pan;
}

float mixer_get_pan(int channel)
{
    assert(channel < mixer.channels_count);
    return mixer.channels[channel]->pan;
}

void mixer_change_eq_low(int channel, float gain)
{
    assert(channel < mixer.channels_count);
    mixer.channels[channel]->eq.lg = gain;
}

void mixer_change_eq_mid(int channel, float gain)
{
    assert(channel < mixer.channels_count);
    mixer.channels[channel]->eq.mg = gain;
}

void mixer_change_eq_high(int channel, float gain)
{
    assert(channel < mixer.channels_count);
    mixer.channels[channel]->eq.hg = gain;
}

float mixer_get_eq_low(int channel)
{
    assert(channel < mixer.channels_count);
    return mixer.channels[channel]->eq.lg;
}

float mixer_get_eq_mid(int channel)
{
    assert(channel < mixer.channels_count);
    return mixer.channels[channel]->eq.mg;
}

float mixer_get_eq_high(int channel)
{
    assert(channel < mixer.channels_count);
    return mixer.channels[channel]->eq.hg;
}

bool init_device_recording()
{
    // Default audio spec
    SDL_AudioSpec desired_spec;
    SDL_zero(desired_spec);
    desired_spec.freq = 44100;
    desired_spec.format = AUDIO_F32;
    desired_spec.channels = 2;
    desired_spec.samples = 4096;
    desired_spec.callback = audio_recording_callback;

    // Open recording device
    recording_device_id = SDL_OpenAudioDevice(
        SDL_GetAudioDeviceName(0, SDL_TRUE),
        SDL_TRUE,                     // is recording?
        &desired_spec,                // what we want
        &recording_spec,              // what we get
        SDL_AUDIO_ALLOW_FORMAT_CHANGE // that's ok
    );

    if (recording_device_id == 0)
    {
        // Report error
        printf("Failed to open recording device! SDL Error: %s", SDL_GetError());
        return false;
    }

    return true;
}

bool init_device_playback()
{
    SDL_AudioSpec desired_spec;
    SDL_zero(desired_spec);
    desired_spec.freq = 44100;
    desired_spec.format = AUDIO_F32;
    desired_spec.channels = 2;
    desired_spec.samples = 4096;
    desired_spec.callback = audio_playback_callback;

    // Open playback device
    playback_device_id = SDL_OpenAudioDevice(
        NULL,           // whatever device you have is fine
        SDL_FALSE,      // playback
        &desired_spec,  // what we want
        &playback_spec, // what we get
        SDL_AUDIO_ALLOW_FORMAT_CHANGE);

    if (playback_device_id == 0)
    {
        // Report error
        printf("Failed to open playback device! SDL Error: %s", SDL_GetError());
        return false;
    }

    return true;
}

bool init_devices()
{
    return init_device_recording() && init_device_playback();
}

void list_devices(bool recording)
{
    int devicesCount = SDL_GetNumAudioDevices(recording);
    const char *mode = recording ? "recording" : "playback";
    SDL_Log("Number of available devices (%s): %d", mode, devicesCount);

    for (int i = 0; i < devicesCount; ++i)
    {
        const char *device_name = SDL_GetAudioDeviceName(i, recording);
        SDL_Log("Device found (%s): %s", mode, device_name);
    }

    SDL_version version;
    SDL_GetVersion(&version);
    SDL_Log("Using SDL version %d.%d.%d", version.major, version.minor, version.patch);
}

channel_t *init_channel()
{
    channel_t *channel = (channel_t *)malloc(sizeof(channel_t));
    channel->audio = NULL;
    channel->volume = 1.0;
    channel->pan = 0.5;
    init_3band_state(&channel->eq, 880, 5000, 480000); // TODO: check correct freq rate
    return channel;
}

bool init_buffers()
{
    mixer.bytes_per_second = get_bytes_per_second();
    // Calculate max buffer use with 1 second margin to the end
    mixer.buffer_byte_max_position = MAX_RECORDING_SECONDS * mixer.bytes_per_second;

    mixer.recording_buffer = init_audio_buffer(RECORDING_BUFFER_SECONDS * mixer.bytes_per_second);

    mixer.channels = (channel_t **)malloc(MAX_CHANNELS * sizeof(channel_t *));

    for (int i = 0; i < MAX_CHANNELS; i++)
    {
        mixer.channels[i] = init_channel();
    }

    mixer.channels_count = MAX_CHANNELS;

    mixer.current_channel_rec = 0;

    mixer.state = MIXER_IDLE;

    return true;
}

void mixer_update()
{
    if (mixer.state == MIXER_IDLE || mixer.state == MIXER_STOP) {
        return;
    }

    if (mixer.state == MIXER_PLAYING) {
        mixer_update_playback();
    }
    else if (mixer.state == MIXER_RECORDING) {
        mixer_update_recording();
    }

}

bool mixer_init()
{
    list_devices(true);
    list_devices(false);

    if (!init_devices())
        return false;

    if (!init_buffers())
        return false;

    return true;
}

void mixer_clean()
{
    // Free playback audio
    if (mixer.recording_buffer != NULL)
    {
        free(mixer.recording_buffer->buffer);
        free(mixer.recording_buffer);
        mixer.recording_buffer = NULL;
    }

    for (int i = 0; i < mixer.channels_count; i++) 
    {
        channel_t *channel = mixer.channels[i];
        if (channel->audio != NULL) {
            free(channel->audio);
        }

        free(channel);
    }
}