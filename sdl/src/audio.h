#ifndef AUDIO_H
#define AUDIO_H

#include <stdint.h>

typedef struct
{
    uint8_t *buffer;
    int buffer_byte_size;
} audio_buffer_t;

audio_buffer_t *init_audio_buffer(int buffer_byte_size);
void reset_audio_buffer(audio_buffer_t *audio);
void release_audio_buffer(audio_buffer_t *audio);

#endif // AUDIO_H