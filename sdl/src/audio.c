#include "audio.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>


void reset_audio_buffer(audio_buffer_t *audio)
{
    memset(audio->buffer, 0, audio->buffer_byte_size);
}

void release_audio_buffer(audio_buffer_t *audio) 
{
    free(audio->buffer);
    free(audio);
}

audio_buffer_t *init_audio_buffer(int buffer_byte_size)
{
    audio_buffer_t *audio = (audio_buffer_t *)malloc(sizeof(audio_buffer_t));

    audio->buffer_byte_size = buffer_byte_size;

    // Allocate and initialize byte buffer
    uint8_t *buffer = (uint8_t *)malloc(audio->buffer_byte_size * sizeof(uint8_t));
    if (!buffer)
    {
        printf("Cannot initialize buffer");
        return 0;
    }

    audio->buffer = buffer;
    reset_audio_buffer(audio);

    return audio;
}