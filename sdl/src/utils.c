#include "utils.h"

#include <math.h>

double clamp(double d, double min, double max)
{
    const double t = d < min ? min : d;
    return t > max ? max : t;
}

double angle_between_points(double x1, double y1, double x2, double y2)
{
    // double dx = x1 - x2;
    double dx = x2 - x1;
    double dy = y2 - y1;
    double angle = atan2(dy, dx);

    if (angle < 0)
    {
        angle = PI - fabs(angle);
        angle += PI;
    }

    return angle;
}

double deg_to_rad(double deg)
{
    // 180 -> PI
    // deg -> rad
    // 180 rad = deg * PI
    // rad = deg * PI / 180
    return deg * (PI / 180.0);
}

double rad_to_deg(double rad)
{
    // 180 -> PI
    // deg -> rad
    // 180 rad = deg * PI
    // deg = 180 rad / PI
    return rad * (180.0 / PI);
}