#ifndef UTILS_H
#define UTILS_H


#define PI 3.14159265358979323846264338

#define CLAMP(x, a, b) ((x) < (a) ? (a) : (x) > (b) ? (b) \
                                                    : (x))


double clamp(double d, double min, double max);

double angle_between_points(double x1, double y1, double x2, double y2);

double deg_to_rad(double deg);
double rad_to_deg(double rad);

#endif // UTILS_H