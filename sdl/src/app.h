#ifndef APP_H
#define APP_H

#include "mixer.h"
#include "SDL2/SDL.h"
#include "kiss/kiss_sdl.h"
#include "widgets.h"
#include "app_model.h"

#define FPS_INTERVAL 1.0 //seconds.

typedef struct {

    Uint32 fps_lasttime; 
    Uint32 fps_current; //the current FPS.
    Uint32 fps_frames; //frames passed since the last recorded fps.

    SDL_Renderer *renderer;

    int quit;
    int draw;

	kiss_window window;
	kiss_array objects;
    kiss_image background_img;
    kiss_menu menu;

	control_button_gui button_play;
	control_button_gui button_record;
	control_button_gui button_backward;
    control_button_gui button_forward;
    control_button_gui button_stop;
    control_button_gui button_pause;

    channel_gui channel1;
    channel_gui channel2;
    channel_gui channel3;
    channel_gui channel4;

    app_model_t app_model;

} app_t;

void app_init(app_t *app);
bool app_init_app(app_t *app);
bool app_init_widgets(app_t *app);
void app_process_input(app_t *app);
void app_update(app_t *app);
void app_render(app_t *app);
void app_clean(app_t *app);

extern app_t *global_app;

#endif // APP_H