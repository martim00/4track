#include "tools.h"
#include "app_model.h"
#include "SDL2/SDL.h"
#include "mixer2.h"
#include "serializer.h"

void open_tool(app_model_t *app) 
{
    SDL_Log("Running open tool");
    serializer_open_file(get_mixer(), "/Users/bucica/workspace/4track/sdl/project");
}

void save_tool(app_model_t *app)
{
    SDL_Log("Running save tool");
    serializer_save_file(get_mixer(), "/Users/bucica/workspace/4track/sdl/project");
}

void play_tool(app_model_t *app)
{
    SDL_Log("Running play tool");
	app->state = STATE_PLAYING;
	mixer_start_playing();
}

void record_tool(app_model_t *app)
{
    SDL_Log("Running record tool");
	app->state = STATE_RECORDING;
	mixer_start_recording();
}

void forward_tool(app_model_t *app)
{
    SDL_Log("Running forward tool (not implemented)");
    app->state = STATE_STOPPED;
}

void backward_tool(app_model_t *app)
{
    SDL_Log("Running backward tool");
    app->state = STATE_STOPPED;
    mixer_return_to_zero();
}

void stop_tool(app_model_t *app)
{
    SDL_Log("Running stop tool");
    app->state = STATE_STOPPED;
    mixer_stop();
}

void pause_tool(app_model_t *app)
{
    SDL_Log("Running pause tool");
    app->state = STATE_PAUSED;
    mixer_pause();
}