#ifndef WIDGET_H
#define WIDGET_H

#include <stdint.h>
#include <stdbool.h>
#include <SDL2/SDL.h>

typedef void (*render_widget_func)(void*);
typedef void (*update_widget_func)(void*);
typedef void (*on_click_widget_func)(void*, int, int);

typedef struct {
    int x;
    int y;
    update_widget_func update;
    render_widget_func render;
    on_click_widget_func on_click;

} widget_t;


// typedef vec_t(widget_t) widget_vec_t;


void widget_update(widget_t* w);
void widget_render(widget_t* w);
void widget_on_click(widget_t* w, int x, int y);



// button

typedef void (*button_onclick_func)(void);

typedef struct {
    widget_t base;
    int width;
    int height;
    uint32_t color;
    bool is_pressed;
    SDL_Texture *texture;
    SDL_Texture *texture_pressed;
    button_onclick_func button_on_click;
} button_t;

button_t *new_button(int x, int y, int width, int height, uint32_t color, 
    char *texture_file, char *texture_pressed_file, void (*button_on_click)());

extern widget_t **widgets;



#endif // WIDGET