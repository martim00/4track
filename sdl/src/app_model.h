#ifndef INCLUDED_APP_MODEL_H
#define INCLUDED_APP_MODEL_H


#define STATE_PLAYING 1
#define STATE_RECORDING 2
#define STATE_PAUSED 3
#define STATE_STOPPED 4


typedef struct {
    int state;
} app_model_t;

#endif 