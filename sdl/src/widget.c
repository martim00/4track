#include "widget.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "display.h"
#include "geometry.h"


widget_t **widgets = NULL;

void widget_update(widget_t* w) {
    if (w->update != NULL) {
        w->update(w);
    }
}

void widget_render(widget_t* w) {
    if (w->render != NULL) {
        w->render(w);
    }
}

void widget_on_click(widget_t *w, int x, int y) {
    if (w->on_click != NULL) {
        w->on_click(w, x, y);
    }
}

// button

void render_button(void* w) {
    button_t* self = (button_t*)w;

    if (self->texture != NULL) {

        if (self->is_pressed && self->texture_pressed != NULL) {
            // SDL_Rect texr; texr.x = self->base.x; texr.y = self->base.y; texr.w = self->width; texr.h = self->height; 
            SDL_Rect texr; texr.x = self->base.x; texr.y = self->base.y; texr.w = self->width; texr.h = self->height; 
            SDL_RenderCopy(renderer, self->texture_pressed, NULL, &texr);
            // draw_rect(self->base.x, self->base.y, self->width, self->height, self->color);
        } else {
            SDL_Rect texr; texr.x = self->base.x; texr.y = self->base.y; texr.w = self->width; texr.h = self->height; 
            SDL_RenderCopy(renderer, self->texture, NULL, &texr);

        }
    } else {
        draw_rect(self->base.x, self->base.y, self->width, self->height, self->color);
    }
    
}

void update_button(void* w) {
    // button_t* self = (button_t*)w;
    // SDL_Log("updating button");
}

bool has_collision(rect_t first, rect_t second) {
    return first.x + first.width >= second.x &&
        second.x + second.width >= first.x &&
        first.y + first.height >= second.y &&
        second.y + second.height >= first.y;
}

void on_click_button(void* w, int x, int y) {
    button_t* self = (button_t*)w;

    rect_t click = { .x = x, .y = y, .width = 1, .height = 1 };
    rect_t button_rect = { .x = self->base.x, .y = self->base.y, .width = self->width, .height = self->height };

    if (has_collision(click, button_rect)) {
        SDL_Log("has collision at %d %d", x, y);
        self->is_pressed = !self->is_pressed;
        SDL_Log("button is pressed %d", self->is_pressed);
        if (self->button_on_click) {
            self->button_on_click();
        }
    }
}

SDL_Texture* load_texture(char *texture_file, int *width, int *height) {
    SDL_Texture *img = NULL;
    img = IMG_LoadTexture(renderer, texture_file);

    if (width != NULL && height != NULL) {
	    SDL_QueryTexture(img, NULL, NULL, width, height); 
    }

    return img;
}

button_t *new_button(
    int x, 
    int y, 
    int width, 
    int height, 
    uint32_t color, 
    char* texture_file, 
    char* texture_pressed_file, 
    void (*button_on_click)()
) {
    button_t *button = malloc(sizeof(button_t));
    button->base.update = &update_button;
    button->base.render = &render_button;
    button->base.on_click = &on_click_button;
    button->base.x = x;
    button->base.y = y;
    button->width = width;
    button->height = height;
    button->color = color;
    button->is_pressed = false;
    button->texture = NULL;
    button->texture_pressed = NULL;
    if (texture_file != NULL) {
        // if passing a texture we are using the texture size as the button size
        button->texture = load_texture(texture_file, &button->width, &button->height);
    }
    if (texture_pressed_file != NULL) {
        button->texture_pressed = load_texture(texture_pressed_file, NULL, NULL);
    }
    button->button_on_click = button_on_click;
    return button;
}