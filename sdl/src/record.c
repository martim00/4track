#include "record.h"

// #define DR_FLAC_IMPLEMENTATION
// #include "../extras/dr_flac.h"  /* Enables FLAC decoding. */
// #define DR_MP3_IMPLEMENTATION
// #include "../extras/dr_mp3.h"   /* Enables MP3 decoding. */
#define DR_WAV_IMPLEMENTATION
#include "miniaudio/dr_wav.h"   /* Enables WAV decoding. */

#define MINIAUDIO_IMPLEMENTATION
#include "miniaudio/miniaudio.h"

#include <stdio.h>

void data_callback(ma_device* pDevice, void* pOutput, const void* pInput, ma_uint32 frameCount)
{
    ma_decoder* pDecoder = (ma_decoder*)pDevice->pUserData;
    if (pDecoder == NULL) {
        return;
    }

    ma_decoder_read_pcm_frames(pDecoder, pOutput, frameCount);

    (void)pInput;
}

// int main(int argc, char** argv)
// {
//     ma_result result;
//     ma_decoder decoder;
//     ma_device_config deviceConfig;
//     ma_device device;

//     if (argc < 2) {
//         printf("No input file.\n");
//         return -1;
//     }

//     result = ma_decoder_init_file(argv[1], NULL, &decoder);
//     if (result != MA_SUCCESS) {
//         printf("Error initing file\n");
//         return -2;
//     }

//     deviceConfig = ma_device_config_init(ma_device_type_playback);
//     deviceConfig.playback.format   = decoder.outputFormat;
//     deviceConfig.playback.channels = decoder.outputChannels;
//     deviceConfig.sampleRate        = decoder.outputSampleRate;
//     deviceConfig.dataCallback      = data_callback;
//     deviceConfig.pUserData         = &decoder;

//     if (ma_device_init(NULL, &deviceConfig, &device) != MA_SUCCESS) {
//         printf("Failed to open playback device.\n");
//         ma_decoder_uninit(&decoder);
//         return -3;
//     }

//     if (ma_device_start(&device) != MA_SUCCESS) {
//         printf("Failed to start playback device.\n");
//         ma_device_uninit(&device);
//         ma_decoder_uninit(&decoder);
//         return -4;
//     }

//     printf("Press Enter to quit...");
//     getchar();

//     ma_device_uninit(&device);
//     ma_decoder_uninit(&decoder);

//     return 0;
// }


// int main(int argc, char** argv)
// {
//     ma_result result;
//     ma_context context;
//     ma_device_info* pPlaybackDeviceInfos;
//     ma_uint32 playbackDeviceCount;
//     ma_device_info* pCaptureDeviceInfos;
//     ma_uint32 captureDeviceCount;
//     ma_uint32 iDevice;

//     if (ma_context_init(NULL, 0, NULL, &context) != MA_SUCCESS) {
//         printf("Failed to initialize context.\n");
//         return -2;
//     }

//     result = ma_context_get_devices(&context, &pPlaybackDeviceInfos, &playbackDeviceCount, &pCaptureDeviceInfos, &captureDeviceCount);
//     if (result != MA_SUCCESS) {
//         printf("Failed to retrieve device information.\n");
//         return -3;
//     }

//     printf("Playback Devices\n");
//     for (iDevice = 0; iDevice < playbackDeviceCount; ++iDevice) {
//         printf("    %u: %s\n", iDevice, pPlaybackDeviceInfos[iDevice].name);
//     }

//     printf("\n");

//     printf("Capture Devices\n");
//     for (iDevice = 0; iDevice < captureDeviceCount; ++iDevice) {
//         printf("    %u: %s\n", iDevice, pCaptureDeviceInfos[iDevice].name);
//     }


//     ma_context_uninit(&context);

//     (void)argc;
//     (void)argv;
//     return 0;
// }