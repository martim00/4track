// #include <stdio.h>

// #include <stdint.h>
// #include <stdbool.h>
// #include <SDL2/SDL.h>
// #include "mixer.h"
// #include "widget.h"
// #include "array.h"
// #include "app.h"

// #include "display.h"

// #define FPS_INTERVAL 1.0 //seconds.

// Uint32 fps_lasttime = 0; 
// Uint32 fps_current; //the current FPS.
// Uint32 fps_frames = 0; //frames passed since the last recorded fps.


// bool is_running = false;
// int previous_frame_time = 0;

// void mouse_press(SDL_MouseButtonEvent event) {
// 	if (event.button == SDL_BUTTON_LEFT){
// 		SDL_Log("Clicked at %d %d", event.x, event.y);
// 		for (int i = 0; i < array_length(widgets); i++) {
// 			widget_on_click(widgets[i], event.x, event.y);
// 		}

//   	}
// }

// void process_input() {
// 	SDL_Event event;
// 	SDL_PollEvent(&event);
	
// 	switch (event.type) {
// 		case SDL_QUIT:
// 			is_running = false;
// 			break;
// 		case SDL_KEYDOWN:
// 			if (event.key.keysym.sym == SDLK_ESCAPE)
// 				is_running = false;
// 			break;

// 		case SDL_MOUSEBUTTONDOWN:
//             mouse_press(event.button);
//             break;
// 	}
// }

// void play_button_clicked() {
// 	play_wave(global_app->loaded_wave);
// }

// void stop_button_clicked() {
// 	pause_wave(global_app->loaded_wave);
// }

// void add_controls() {

// 	int control_height = 100;
// 	int x_center = window_width / 2;

// 	int control_x = x_center - 200;
// 	int control_y = window_height - control_height;

// 	widget_t *button = (widget_t*)new_button(
// 		control_x, 
// 		control_y, 
// 		30, 
// 		control_height, 
// 		0xFFFF0000, 
// 		"assets/play.png",
// 		"assets/play_pressed.png",
// 		&play_button_clicked
// 	);

// 	array_push(widgets, button);

// 	// widget_t *button2 = (widget_t*)new_button(
// 	// 	control_x + 30, 
// 	// 	control_y, 30, 
// 	// 	control_height, 
// 	// 	0x00000000, 
// 	// 	NULL,
// 	// 	&stop_button_clicked
// 	// );

// 	// array_push(widgets, button2);
// }

// void setup(void) {
//     // Allocate the required memory in bytes to hold the color buffer
//     color_buffer = (uint32_t*)malloc(sizeof(uint32_t) * window_width * window_height);

//     // Creating a SDL texture that is used to display the color buffer
//     color_buffer_texture = SDL_CreateTexture(
//         renderer,
//         SDL_PIXELFORMAT_ARGB8888,
//         SDL_TEXTUREACCESS_STREAMING,
//         window_width,
//         window_height
//     );

// 	add_controls();

// 	fps_lasttime = SDL_GetTicks(); //the last recorded time.


// 	// vec_push(&widgets, button);


//     // Loads the vertex and face values for the mesh data structure
//     // load_cube_mesh_data();
//     // load_obj_file_data("./assets/cube.obj");
// 	global_app->loaded_wave = load_waveform("sample.wav");
// 	if (global_app->loaded_wave == NULL) {
// 		exit(-1);
// 	}
// }



// void render(void) {

//     // SDL_RenderClear(renderer);
// 	// SDL_SetRenderDrawColor(renderer, 21, 21, 21, 255);

// 	// draw_grid();
// 	// draw_line(0, 0, 200, 200, 0xFF00FF00);
// 	// draw_rect(10, 300, window_width - 20, 250, 0xFF000000);
// 	// draw_rect(50, 500, 10, 10, 0xFF0000FF);

// 	// draw_waveform(current_stream);
// 	// draw_waveform(loaded_wave);
// 	// draw_cursor(player_cursor);
// 	// draw_controls();

// 	fps_frames++;
// 	if (fps_lasttime < SDL_GetTicks() - FPS_INTERVAL*1000)
// 	{
// 		fps_lasttime = SDL_GetTicks();
// 		fps_current = fps_frames;
// 		fps_frames = 0;
// 	}
// 	// SDL_Log("FPS: %d", fps_current);

// 	for (int i = 0; i < array_length(widgets); i++) {
// 		widget_render(widgets[i]);
// 	}

// 	// render_color_buffer();

//     // clear_color_buffer(0xFF00F0FF);

//     SDL_RenderPresent(renderer);
// 	// printf("Render");
// }

// void update(void) {
//     // Wait some time until the reach the target frame time in milliseconds
//     int time_to_wait = FRAME_TARGET_TIME - (SDL_GetTicks() - previous_frame_time);

//     // Only delay execution if we are running too fast
//     if (time_to_wait > 0 && time_to_wait <= FRAME_TARGET_TIME) {
//         SDL_Delay(time_to_wait);
//     }

//     previous_frame_time = SDL_GetTicks();



// 	for (int i = 0; i < array_length(widgets); i++) {
// 		widget_update(widgets[i]);
// 	}

// }

// void free_resources() {
// 	for (int i = 0; i < array_length(widgets); i++) {
// 		free(widgets[i]);
// 	}
// 	array_free(widgets);
// }


// // int main(void) {

// //    	is_running = initialize_window();
// // 	initialize_mixer();

// // 	setup();

// // 	// play_wave("sample.wav");

// // 	while (is_running) {

// // 		// SDL_Log("%s", raw_stream);

// // 		process_input();
// // 		update();
// // 		render();
// // 	}

// // 	destroy_mixer();
// // 	destroy_window();

// //    	return 0;
// // }

