module github.com/martim00/4track

go 1.14

require (
	github.com/gdamore/tcell v1.1.1
	github.com/gen2brain/malgo v0.0.0-20190415221052-9100cfc8572b
	github.com/gizak/termui/v3 v3.0.0 // indirect
	github.com/jroimartin/gocui v0.4.0
	github.com/krig/go-sox v0.0.0-20180617124112-7d2f8ae31981
	github.com/mum4k/termdash v0.8.0
	github.com/nsf/termbox-go v0.0.0-20190325093121-288510b9734e // indirect
	github.com/rivo/tview v0.0.0-20190406182340-90b4da1bd64c
	github.com/rivo/uniseg v0.0.0-20190313204849-f699dde9c340 // indirect
	github.com/veandco/go-sdl2 v0.4.4 // indirect
)
