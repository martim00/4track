package main

import (
	"context"
	"fmt"
	"time"

	"github.com/martim00/4track/lib"

	"github.com/mum4k/termdash"
	"github.com/mum4k/termdash/align"
	"github.com/mum4k/termdash/cell"
	"github.com/mum4k/termdash/container"
	"github.com/mum4k/termdash/container/grid"
	"github.com/mum4k/termdash/linestyle"
	"github.com/mum4k/termdash/terminal/termbox"
	"github.com/mum4k/termdash/terminal/terminalapi"
	"github.com/mum4k/termdash/widgets/button"
	"github.com/mum4k/termdash/widgets/gauge"
	"github.com/mum4k/termdash/widgets/text"
)

var c container.Container

// rootID is the ID assigned to the root container.
const rootID = "root"

// redrawInterval is how often termdash redraws the screen.
const redrawInterval = 250 * time.Millisecond

// type trackView struct {
// 	armRec bool
// }

type viewModel struct {
	recording bool
	playing   bool
	// tracks          []*trackView
	mixer           *lib.Mixer
	stopRecChannel  chan bool
	stopPlayChannel chan bool
	statusMsg       string
}

func buildTracks(c *container.Container, v *viewModel) ([]container.Option, error) {
	trackRow := []grid.Element{}
	for i, t := range v.mixer.Tracks {

		index := i
		track := t

		armColor := 200
		if track == v.mixer.ArmedTrack {
			armColor = 109
		}
		arm, err := button.New(
			"",
			func() error {
				// t.armRec = !t.armRec
				v.mixer.ArmRecord(index)
				updateLayout(c, v)
				return nil
			},
			button.GlobalKey('a'),
			button.Width(2),
			button.Height(1),
			button.FillColor(cell.ColorNumber(armColor)),
		)

		placeholder, err := button.New(
			"",
			func() error {
				return nil
			},
			button.Width(2),
			button.Height(1),
			button.FillColor(cell.ColorNumber(226)),
		)

		panicOnError(err)

		absolute, err := gauge.New(
			gauge.Height(1),
			gauge.Color(cell.ColorBlue),
			gauge.Border(linestyle.Light),
			gauge.BorderTitle("db"),
		)
		panicOnError(err)

		trackRow = append(trackRow, grid.ColWidthPerc(25,
			grid.RowHeightPerc(20,
				grid.Widget(absolute,
					container.Border(linestyle.Light),
					container.MarginLeftPercent(20),
					container.MarginRightPercent(20),
					// container.AlignVertical(align.VerticalBottom),
				),
			),
			grid.RowHeightPerc(80,
				grid.ColWidthPerc(99,
					grid.RowHeightPerc(25,
						grid.Widget(placeholder,
							container.Border(linestyle.Light),
							container.MarginLeftPercent(20),
							container.MarginRightPercent(20),
							container.AlignVertical(align.VerticalBottom),
						),
					),
					grid.RowHeightPerc(25,
						grid.Widget(arm,
							// container.BgColor(cell.ColorYellow),
							container.Border(linestyle.Light),
							container.MarginLeftPercent(20),
							container.MarginRightPercent(20),
							container.AlignVertical(align.VerticalBottom),
						),
					),
				),
			),
		))
	}

	builder := grid.New()
	builder.Add(
		grid.RowHeightPerc(99,
			trackRow...,
		),
	)

	gridOpts, err := builder.Build()
	gridOpts = append(gridOpts, container.MarginLeftPercent(20))
	gridOpts = append(gridOpts, container.MarginRightPercent(20))

	if err != nil {
		return nil, err
	}
	return gridOpts, nil
}

func buildStatusBar(c *container.Container, v *viewModel) ([]container.Option, error) {
	options := []container.Option{}

	status, err := text.New()
	if err != nil {
		return options, err
	}

	msg := "status..."
	if len(v.statusMsg) > 0 {
		msg = v.statusMsg
	}

	if err := status.Write(msg); err != nil {
		return options, err
	}

	options = append(options, container.Border(linestyle.Light))
	options = append(options, container.BorderTitle("Status"))
	options = append(options, container.PlaceWidget(status))
	return options, nil
}

func updateStatusBar(c *container.Container, v *viewModel, msg string) {
	v.statusMsg = msg
	updateLayout(c, v)
}

func buildControls(c *container.Container, v *viewModel) ([]container.Option, error) {

	recColor := 202
	if v.recording {
		recColor = 102
	}

	rec, err := button.New(
		"(r)ec",
		func() error {
			if !v.mixer.HasArmedRecord() {
				updateStatusBar(c, v, "You should arm record")
				return nil
			}

			v.recording = !v.recording
			updateLayout(c, v)

			go v.mixer.Record(v.stopRecChannel)

			msg := "recording..."
			updateStatusBar(c, v, msg)
			return nil
		},
		button.GlobalKey('r'),
		button.FillColor(cell.ColorNumber(recColor)),
	)
	panicOnError(err)

	play, err := button.New("(p)lay",
		func() error {
			if v.recording || v.playing {
				return nil
			}
			v.playing = true
			mixed := v.mixer.Mix()
			if !mixed {
				updateStatusBar(c, v, "nothing to play")
				return nil
			}
			stoppedChan, progressChan := v.mixer.PlayMixed(v.stopPlayChannel)
			go func() {
				for {
					select {
					case <-stoppedChan:
						v.playing = false
						updateStatusBar(c, v, "stopped playing")
						return
					case progress := <-progressChan:
						updateStatusBar(c, v, progress)
					}

				}

			}()
			return nil
		},
		button.GlobalKey('p'),
		button.FillColor(cell.ColorNumber(220)),
	)
	panicOnError(err)

	stop, err := button.New("(s)top",
		func() error {
			if !v.recording {
				return nil
			}
			if !v.mixer.HasArmedRecord() {
				updateStatusBar(c, v, "You should arm record")
				return nil
			}
			v.recording = false
			v.stopRecChannel <- true
			updateLayout(c, v)
			updateStatusBar(c, v, fmt.Sprintf("stopped recording %d samples", v.mixer.ArmedTrack.SamplesCount))
			return nil
		},
		button.GlobalKey('s'),
		button.FillColor(cell.ColorNumber(220)),
	)
	panicOnError(err)

	pause, err := button.New("(p)ause",
		func() error {
			return nil
		},
		button.GlobalKey('p'),
		button.FillColor(cell.ColorNumber(220)),
	)
	panicOnError(err)

	builder := grid.New()
	builder.Add(
		grid.RowHeightPerc(99,
			grid.ColWidthPerc(25,
				grid.Widget(rec),
			),
			grid.ColWidthPerc(25,
				grid.Widget(play),
			),
			grid.ColWidthPerc(25,
				grid.Widget(stop),
			),
			grid.ColWidthPerc(25,
				grid.Widget(pause),
			),
		),
	)

	gridOpts, err := builder.Build()
	if err != nil {
		return nil, err
	}
	gridOpts = append(gridOpts, container.MarginLeftPercent(20))
	gridOpts = append(gridOpts, container.MarginRightPercent(40))
	gridOpts = append(gridOpts, container.Border(linestyle.Light))
	return gridOpts, nil
}

func panicOnError(err error) {
	if err != nil {
		panic(err)
	}
}

func renderLayout(c *container.Container, v *viewModel) []container.Option {
	controls, err := buildControls(c, v)
	panicOnError(err)

	tracks, err := buildTracks(c, v)
	panicOnError(err)

	statusBar, err := buildStatusBar(c, v)
	panicOnError(err)

	layout := []container.Option{

		container.Border(linestyle.Light),
		container.SplitHorizontal(
			container.Top(
				tracks...,
			),
			container.Bottom(
				container.SplitVertical(
					container.Left(statusBar...),
					container.Right(controls...),
					container.SplitPercent(15),
				),
			),
			container.SplitPercent(80),
		),
	}

	return layout
}

func updateLayout(c *container.Container, v *viewModel) {
	if err := c.Update(rootID, renderLayout(c, v)...); err != nil {
		panic(err)
	}
}

// func newTrackView() *trackView {
// 	return &trackView{
// 		armRec: false,
// 	}
// }

func main() {
	t, err := termbox.New(termbox.ColorMode(terminalapi.ColorMode256))
	panicOnError(err)
	defer t.Close()

	c, err := container.New(t, container.ID(rootID))
	if err != nil {
		panic(err)
	}

	ctx, cancel := context.WithCancel(context.Background())

	v := viewModel{
		stopRecChannel:  make(chan bool),
		stopPlayChannel: make(chan bool),
		mixer:           lib.NewMixer(),
		recording:       false,
		playing:         false,
		// tracks: []*trackView{
		// 	newTrackView(),
		// 	newTrackView(),
		// 	newTrackView(),
		// 	newTrackView(),
		// },
	}
	updateLayout(c, &v)

	// if err := c.Update(rootID, gridOpts...); err != nil {
	// 	panic(err)
	// }

	quitter := func(k *terminalapi.Keyboard) {
		if k.Key == 'q' || k.Key == 'Q' {
			cancel()
		}
	}
	if err := termdash.Run(ctx, t, c, termdash.KeyboardSubscriber(quitter), termdash.RedrawInterval(redrawInterval)); err != nil {
		panic(err)
	}
}

// periodic executes the provided closure periodically every interval.
// Exits when the context expires.
func periodic(ctx context.Context, interval time.Duration, fn func() error) {
	ticker := time.NewTicker(interval)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			if err := fn(); err != nil {
				panic(err)
			}
		case <-ctx.Done():
			return
		}
	}
}

// rotateFloats returns a new slice with inputs rotated by step.
// I.e. for a step of one:
//   inputs[0] -> inputs[len(inputs)-1]
//   inputs[1] -> inputs[0]
// And so on.
func rotateFloats(inputs []float64, step int) []float64 {
	return append(inputs[step:], inputs[:step]...)
}
